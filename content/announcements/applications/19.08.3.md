---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: KDE Ships Applications 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE Ships Applications 19.08.3
version: 19.08.3
---

{{% i18n_date %}}

{{% i18n_var "Today KDE released the third stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../19.08.0" "19.08" %}}

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello, among others.

Improvements include:

- In the video-editor Kdenlive, compositions no longer disappear when reopening a project with locked tracks
- Okular's annotation view now shows creation times in local time zone instead of UTC
- Keyboard control has been improved in the Spectacle screenshot utility
