---
qtversion: 5.15.2
date: 2022-09-12
layout: framework
libCount: 83
---


### Breeze Icons

* Update chatroom names in the README

### Extra CMake Modules

* ECMGenerateExportHeader: fix & document CUSTOM_CONTENT_FROM_VARIABLE
* Work around exiv2 still using std::auto_ptr
* Add OpenBSD support
* Sync QT_MIN_VERSION with KF's REQUIRED_QT_VERSION
* [FindXCB] Remove special treatment for XINPUT

### KArchive

* ktar fix underflow

### KAuth

* Fix polkit-qt-1 dependency

### KCalendarCore

* Ensure that status values are valid
* Fix nesting of update()/updated() calls

### KCMUtils

* Use a CMake option() for the TOOLS_ONLY flag
* KPluginSelector: Remove unnecessary background rect in About sheet (bug 457648)
* Set Type=Application for generated KCM desktop file (bug 457673)
* kcmoduleqml: don't unnecessarily set setContentsMargins twice

### KConfig

* Add KServiceAction as a friend of KConfigGroup
* KConfigWatcher: initialize d->m_config in constructor

### KConfigWidgets

* Deduplicate color loading code
* [KCommandBar] Add action to clear command history

### KCoreAddons

* Remove extra semi colon
* Fix KUrlMimeData::exportUrlsToPortal for mixed files-and-directories URIs

### KDeclarative

* KQuickAddons::ConfigModule: Call QmlObject constructor with shared pointer directly
* KDeclarative::setupEngine: Use KQuickIconProvider
* Deprecate KDeclarative::setupEngine
* Do not build kpackagelauncherqml when building without deprecations
* Deprecate KPackage related methods in QmlObject
* fallbacktaphandler remove duplicate parentheses
* Deprecate KDeclarative::setupQmlJsDebugger
* KDeclarative::setupEngine: Wrap code that uses deprecated method in deprecation wrappers
* Deprecate KDeclarative::runtimePlatform and related methods

### KDED

* Don't install service type definition when building against Qt6

### KGlobalAccel

* GlobalShortcutsRegistry: use std::unique_ptr to manage Components
* Let GlobalShortcutsRegistry manage all components
* Use KFileUtils::findAllUniqueFiles for listing desktop files
* KGlobalAccelD: remove init() function
* GlobalShortcutsRegistry: add two methods providing info about components
* Set QGuiApplication::desktopSettingsAware to false
* Treat key sequence string as PortableText when parsing
* Avoid iterating a container while it's being mutated (bug 437364)
* Don't set NoDisplay when cleaning up service components (bug 454396)
* Don't pass Exec arguments to kstart when launching via desktop entry name (bug 440507)

### KDE GUI Addons

* Make QtX11Extras required when building with X11 support (bug 458290)

### KHolidays #

* Turn HolidaysRegionsModel into a table model

### KHTML

* Add CMake option to build WITH_X11

### KIconThemes

* Remove unused KItemViews dependency
* Import KIconProvider from KDeclarative
* KIconTheme: fix if condition
* KIconLoader: remove unneeded calls to d->initIconThemes()
* KIconLoader: call methods in Private class constructor
* KIconButton: Add tooltip

### KImageFormats

* Protect against too big resize for a QByteArray

### KInit

* Add CMake option to build WITH_X11

### KIO

* Install WidgetsAskUserActionHandler header
* file_unix: optimize copy (bug 458001)
* KFilePlacesItem: Remove unused enum
* KFilePlacesModel: Don't show error message on UserCanceled
* Add overloads for Utils::concatPaths()
* Use AskUserActionInterface (async) in UserNotificationHandler (bug 451834)
* Ensure iconForStandardPath() returns user-home for QStandardPaths::HomeLocation (bug 447238)
* trash:/ set the UDS_LOCAL_PATH (bug 368104)
* KUrlNavigator: check if typed text matches a relative dir first (bug 353883)
* FileUndoManager: for copyjob only add undo if it copied something (bug 454226)
* Use Functors with QMetaObject::invokeMethod()
* DesktopExecParser: don't kioexec if there is a handler for scheme (bug 442721)
* force admin worker to run in a thread
* install workerfactory header
* introduce RealWorkerFactory
* revise jobuidelegate factorization
* [kfileitemactions] Show desktop file actions more prominently (bug 417012)
* clear state after timeout'd special call
* make loading UDSEntries from streams thread safe
* Remove ServiceType from KDED metadata
* Drop obsolete X11 dependency, introduce WITH_X11 option instead
* Don't install service type definition for properties plugins when building without deprecated things
* Remove service type definition for DnD plugins
* Sync QT_MIN_VERSION with KF's REQUIRED_QT_VERSION

### Kirigami

* Make sure we are not showing the pull indicator while idle
* Workaround apps that assume flickable exists before component completition (bug 458099)
* settingscomponents: Ensure page header is drawn for mobile
* Add license info to tst_formlayout
* Fix a potential crash in imagecolors
* FormLayout: Explicitly round up implicit sizes (bug 457675)
* ActionToolBar: Fix undefined children error with QQC2 Action
* do not use uppercase letter
* improve doc wording
* improve doc wording
* Don't leak a QQuickImageResponse in Kirigami::Icon
* Standardize ActionButton comment styling

### KJobWidgets

* kuiserverv2jobtracker: Don't terminate a null JobView

### KNewStuff

* [qtquickdialogwrapper] Fix finding parent window
* Render GIFs in entry details (bug 458046)
* Fix appstream id for vokoscreenNG (bug 458064)

### KQuickCharts

* Use backend-agnostic QSGGeometry attribute types
* PieChart: fix data normalization

### KRunner

* Fix renaming of plugin ID when converting DBus runner metadata files (bug 456562)

### KTextEditor

* add token description for message
* add showMessage, to show formatted messages
* add addWidget to MainWindow
* Fix autobracket insertion for multicursors
* Improve autocompletion with multicursors
* Use Ctrl+Shift+Alt+V for paste dialog
* Apply title capitalization to all actions
* Improve singleline selection commenting (bug 456819)
* avoid shortcut clash, we can think about this later
* Revert changes to JS API
* Inline quickdialog
* Keep dialog open when clicking into editor field
* Double click to paste
* Enable clipboard history when non read-only
* Add config option: max clipboard history entries
* Remove fuzzy matching
* Add dialog for clipboard history
* Remove KatePasteMenu
* Move QuickDialog class from Kate here
* Implement mouse selection with multicursor
* Fix toggle comment with empty line in selection (bug 458126)
* Vi input: fix find selected
* Fix some inconsistencies in completion config tab (bug 443994)
* Dont enforce word boundary with multicursor occurunce select (bug 457948)
* Optionally allow to show EndOfLine type in statusbar (bug 457885)
* Only update visibility if needed
* Fix Goto line in clipboard
* Make statusbar configurable
* don't promote KService, we want to be used like a proper library
* don't install own service types for KF6
* remove useless include
* use KFontRequester
* allow to configure printing font (bug 457480)
* Fix action texts
* using default constructor for empty string
* Multicursor: Allow skipping selected occurunce
* Update folding ranges on text modification (bug 436480)
* Ignore folding ranges if document has changed (bug 384459)
* KateDocument: always add space after single line comment start marker (bug 456819)
* Use an enum instead of an int to indicate {un,toggle}comment change
* remember last pattern for the "Align On..." action dialog
* rename "Align" to "Format Indentation" in config tooltip
* Making alignOn support block selection mode + fix offset bug
* make view.searchText() return value type consistent, using an invalid range rather than null when it fails to find the text
* Fix cursor position after calling an each-based command, when possible
* Fix vi mode delete behavior (bug 453964)
* document.alignOn defaults to empty pattern
* defaults to foward search
* backward -> backwards, fixing english mistake and using same naming convention as everywhere else in the project
* implements alignOn + rename align to formatIndent (on the surface only)
* making exposed searchText function more general and simplifying JS
* using native textSearch rather than implementing it in JS
* renaming to more efficient command names + adding help text
* fix behavior for shift+arrows after the selection has been made
* Add selection commands (useful for keyboard macros in particular)
* new editing command proposal: align

### KWallet Framework

* Don't register dummy org.freedesktop.secrets service when api is disabled (bug 458069)
* Only build kwallet-query's manpage if it's enabled
* Add missing cerrno header
* backendpersisthandler parentheses around assignment used as truth value

### KWayland

* Auto cleanup the dangling KWayland::Client::Surface returned from fromWindow

### KWidgetsAddons

* Fix KMessageBoxes buttons not having the contents they should the first time
* Improve code quality
* Support dates in ISO format
* Avoid usage of ambiguous two-digit year in date picker
* Share helper returning 4-digit date format with other classes
* only remove ... not &
* Paint frame before contents (bug 454031)
* ensure consistent ... removal in iconText (bug 428372)
* KMessageWidget: Ensure correct tab order of action buttons

### KXMLGUI

* Fix crash in addActionToSchemesMoreButton() introduced by merge mistake

### Plasma Framework

* Fix dataengine loading in Qt6 code path
* PC3/Slider: Enable hover events (bug 454714)
* PlasmaDialog: Don't constantly reset cursor when resizable edges are ON (bug 454714)
* PC3: Support RTL layouts in controls
* extras/ListItem: Fix undefined reference error to properties from newer controls
* PC3: Improve support for RTL sliders
* extras/Highlight: Fix code style and explicitness
* Port from KDeclarative::runtimePlatform to KRuntimePlatform::runtimePlatform
* BusyIndicator: Fix initialization of the running property
* extras/ExpandableListItem: Remove 'currentIndex' state when unhovering
* PC3/CheckBox: Fix hover animation repeated on press
* TextField: fix height change when password dot appears (bug 410810)
* ExpandableListItem: add `expanded` property to indicate the view is visible
* Synchronise busy indicator start points
* datamodel.cpp 'then' statement is equivalent to the 'else'
* a11y: toolbuttons are pressable
* a11y: roundbuttons are buttons too

### Prison

* Add the mandated 10x quiet zones for Code 39 and Code 93 barcodes

### Purpose

* kdeconnect: Fix name role
* Port the remaining QQC1 uses

### QQC2StyleBridge

* Explicit binding for visible (bug 458486)
* Make DelayButton look like Spectacle's ProgressButton
* Add FIXME about ToolTip instances with child items that use anchors

### Solid

* Add explanation for enum value 'Smb3'
* Improve SMB3 filesystem integration
* Add Samba's SMB3 filesystem to Solid

### Syntax Highlighting

* cmake.xml: Add `CMAKE_FILES_DIRECTORY` as undocumented
* Use C-style indenting for GLSL
* bash.xml: Reformat for better looking
* VHDL: fix some highlighting and folding errors
* Python: when strings are not closed, mark only the following line as error
* Change singleLineComments to be AfterWhiteSpace for c-like langs (bug 438744 456819)
* Indexer: ignore attribute with lookAhead in suggestRuleMerger() and check attribute only used with lookAhead or IncludeRules
* detect .gcode files as G-Code (bug 457753)
* Asciidoc: fix section block continuation that should not be highlighted
* Asciidoc: optimize rendering time by about 35%
* Asciidoc: fix title highlighting when not starting at level 0
* Bash/Zsh: add -o and -a logical operators with [ ... ]
* G-Code: fix number pattern, Mnnn as M-word or M-word (user), add String and.. (bug 457754)
* Fortran: Interpret percent sign as operator
* RPM Spec: fix #20: support of ${expand:...}, ${lua:...}, lua section and some..
* VHDL: (#21) fix instantiating component in architecture
* CartoCSS: fix the last statement in curly braces when it does not terminate with a semicolon
* C: support of C23 and synchronization with cpp.xml
* C++: Symbol style for LineContinue in String and # in Define
* C++: add assume attribute
* C++: add z/Z number suffix
* C++: add extensions for printf (C23): wN, wfN, H, D, and DD length modifiers, b format
* C++: add elifdef/elifndef ; add Char/String Literal Prefix style ; check extra hexadecimal char in String8
* Optimize AbstractHighlighter
* Resolve regexp rules lazily

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
