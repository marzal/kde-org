---
aliases:
- ../../plasma-5.12.0
changelog: 5.11.5-5.12.0
date: 2018-02-06
layout: plasma
title: KDE Plasma 5.12.0 LTS, Speed. Stability. Simplicity.
youtube: xha6DJ_v1E4
figure:
  src: /announcements/plasma/5/5.12.0/plasma-5.12.png
  class: text-center mt-4
---

{{% i18n_date %}}

{{% i18n_var "Today KDE releases a %[1]s update to Plasma 5, versioned %[2]s" "Feature" "5.12.0." %}}

Plasma 5.12 LTS is the second long-term support release from the Plasma 5 team. We have been working hard, focusing on speed and stability for this release. Boot time to desktop has been improved by reviewing the code for anything which blocks execution. The team has been triaging and fixing bugs in every aspect of the codebase, tidying up artwork, removing corner cases, and ensuring cross-desktop integration. For the first time, we offer our Wayland integration on long-term support, so you can be sure we will continue to provide bug fixes and improvements to the Wayland experience.

## New in Plasma 5.12 LTS

### Smoother and Speedier

Speed and memory improvements are the focus of this long-term release. The new Plasma LTS uses less CPU and less memory than previous versions. The process of starting the Plasma desktop is now significantly faster.

### New Features

{{<figure src="/announcements/plasma/5/5.12.0/weather-applet.png" alt="Weather Applet with Temperature " class="text-center" width="600px" caption="Weather Applet with Temperature">}}

{{<figure src="/announcements/plasma/5/5.12.0/system-monitor.png" alt="CPU usage in System Activity " class="text-center" width="600px" caption="CPU usage in System Activity">}}

{{<figure src="/announcements/plasma/5/5.12.0/window-shadows.png" alt="Larger, horizontally-centered window shadows " class="text-center" width="600px" caption="Larger, horizontally-centered window shadows">}}

The list of new features in Plasma 5.12 LTS doesn't stop with improved performance. You can also look forward to the following:

- Wayland-only Night Color feature that lets you adjust the screen color temperature to reduce eye strain
- Usability improvement for the global menu: adding a global menu panel or window decoration button enables it without needing an extra configuration step
- KRunner can now be completely used with on-screen readers such as Orca
- Notification text is selectable again and allows you to copy links from notifications
- The weather applet can now show the temperature next to the weather status icon on the panel
- Clock widget's text is now sized more appropriately
- System Activity and System Monitor display per-process graphs for the CPU usage
- Windows shadows are horizontally centered and larger by default
- The Properties dialog now shows file metadata
- The Icon applet now uses favicons for website shortcuts
- The Kickoff application menu has an optimized layout

### Discover

{{<figure src="/announcements/plasma/5/5.12.0/discover-app.png" alt="Discover's new app page " class="text-center" width="600px" caption="Discover's new app page">}}

We have made many improvements to the Discover user interface, starting with redesigned app pages to showcase all the amazing software you can install. Non-browsing sections of the interface now have leaner headers, and browsing views are more compact, allowing you to see more apps at once. The screenshots are bigger and support keyboard navigation. The list of installed apps is sorted alphabetically, and the interface for configuring sources has been polished.

Discover has also seen major improvements in stability, as well as in Snap and Flatpak support. It supports <tt>apt://</tt> URLs, and will notify you when a package requires a reboot after installation or update. Distributions using Discover can enable offline updates, and the distro upgrade feature can be used to get new distribution releases. Discover comes with better usability on phone form factors (uses Kirigami main action, and has a view specific for searching), and it integrates PackageKit global signals into notifications. All in all, the process of maintaining your software with Discover will feel much smoother.

### Continuous Updates for Plasma on Wayland

{{<figure src="/announcements/plasma/5/5.12.0/kscreen-wayland.png" alt="Display Setup Now Supports Wayland " class="text-center" width="600px" caption="Display Setup Now Supports Wayland">}}

Plasma's support for running on Wayland continues to improve, and it is now suitable for a wider range of testing. It is included as a Long Term Support feature for the first time, which means we will be fixing bugs throughout the entire 5.12 LTS series. New features include:

- Ability to set output resolution and enable/disable outputs through KScreen
- Screen rotation
- Automatic screen rotation based on orientation sensor
- Automatic touch screen calibration
- Fullscreen option for Wayland windows
- Automatic selection of the Compositor based on the used platform

Plasma on Wayland cares about your vision, so an exclusive, Wayland-only feature called Night Color is also available. Night Color integrates with KWin and removes blue light from your screen at night-time, functioning as an equivalent of the great Redshift app that works on X.

We have also started implementing window rules on Wayland and made it possible to use real-time scheduling policy to keep the input responsive. XWayland is no longer required to run the Plasma desktop; however, the applications only supporting X still make use of it.

For those who know their Wayland internals, the protocols we added are:

- xdg_shell_unstable_v6
- xdg_foreign_unstable_v2
- idle_inhibit_unstable_v1
- server_decoration_palette
- appmenu
- wl_data_device_manager raised to version 3

There is also an important change of policy: 5.12 is the last release which sees feature development in KWin on X11. With 5.13 onwards, only new features relevant to Wayland are going to be added.

We have put a lot of work into making Wayland support in Plasma as good as possible, but there are still some missing features and issues with particular hardware configurations. Therefore, we don't yet recommend it for daily use. More information is available on the <a href='https://community.kde.org/Plasma/Wayland_Showstoppers'>Wayland status wiki page</a>.

## What’s New Since Plasma 5.8 LTS

If you have been using our previous LTS release, Plasma 5.8, you might notice some interesting changes when you upgrade to Plasma 5.12. All these great features are waiting for you:

## Previews in Notifications

{{<figure src="/announcements/plasma/5/5.12.0/spectacle-notification.png" alt="Preview in Notifications " class="text-center" width="600px" caption="Preview in Notifications">}}

The notification system gained support for interactive previews that allow you to quickly take a screenshot and drag it into a chat window, an email composer, or a web browser form. This way you never have to leave the application you’re currently working with.

## Task Manager Improvement

{{<figure src="/announcements/plasma/5/5.12.0/mute.png" alt="Audio Icon and Mute Button Context Menu Entry " class="text-center" width="600px" caption="Audio Icon and Mute Button Context Menu Entry">}}

Due to popular demand, we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. Is some app making noise in the background while you are trying to focus on one task? Applications currently playing audio are now marked with an icon, similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.

## Global Menus

{{<figure src="/announcements/plasma/5/5.12.0/global-menus-window-bar.png" alt="Global Menu Screenshots, Applet and Window Decoration " class="text-center" width="600px" caption="Global Menu Screenshots, Applet and Window Decoration">}}

Global Menus have returned. KDE's pioneering feature to separate the menu bar from the application window allows for a new user interface paradigm, with either a Plasma Widget showing the menu, or with the menu neatly tucked away in the window title bar. Setting it up has been greatly simplified in Plasma 5.12: as soon as you add the Global Menu widget or title bar button, the required background service gets started automatically. No need to reload the desktop or click any confirmation buttons!

## Spring-Loaded Folders

{{<figure src="/announcements/plasma/5/5.12.0/spring_loading.gif" alt="Spring Loading in Folder View " class="text-center" width="600px" caption="Spring Loading in Folder View">}}

Folder View is now the default desktop layout. After some years of shunning icons on the desktop, we have accepted the inevitable and changed to Folder View as the default desktop. This brings some icons to the desktop by default, and allows you to put whatever files or folders you want on it for easy access. To make this work, many improvements have been made to Folder View. Spring Loading makes drag-and- dropping files powerful and quick, and there is a tighter icon grid, along with massively improved performance.

## Music Controls in Lock Screen

{{<figure src="/announcements/plasma/5/5.12.0/lock-music.png" alt="Media Controls on Lock Screen " class="text-center" width="600px" caption="Media Controls on Lock Screen">}}

Media controls have been added to the lock screen. For added privacy, they can be disabled in Plasma 5.12. Moreover, music will automatically pause when the system suspends.

## New System Settings Interface

{{<figure src="/announcements/plasma/5/5.12.0/system-settings.png" alt="New Look System Settings " class="text-center" width="600px" caption="New Look System Settings">}}

We introduced a new System Settings user interface for easy access to commonly used settings. It is the first step in making this often-used and complex application easier to navigate and more user-friendly. The new design is added as an option, so users who prefer the older icon or tree views can move back to their preferred way of navigation.

## Plasma Vaults

{{<figure src="/announcements/plasma/5/5.12.0/plasma-vault.png" alt="Plasma Vaults " class="text-center" width="600px" caption="Plasma Vaults">}}

KDE has a focus on privacy. After all, our vision is a world in which everyone has control over their digital life and enjoys freedom and privacy.

For users who often deal with sensitive, confidential and private information, the new Plasma Vault feature offers strong encryption options presented in a user-friendly way. It allows you to lock and encrypt sets of documents, and hide them from prying eyes even when you are logged in. Plasma Vault extends Plasma's Activities feature with secure storage.

## Plasma's Comprehensive Features

Take a look at what Plasma offers - a comprehensive selection of features unparalleled in any desktop software.

### Desktop Widgets

{{<figure src="/announcements/plasma/5/5.12.0/widgets.png" alt="Desktop Widgets " class="text-center" width="600px" caption="Desktop Widgets">}}

Cover your desktop in useful widgets to keep you up to date with weather, amuse you with comics, or help you with calculations.

### Get Hot New Stuff

{{<figure src="/announcements/plasma/5/5.12.0/ghns.png" alt="Get Hot New Stuff " class="text-center" width="600px" caption="Get Hot New Stuff">}}

Download wallpapers, window styles, widgets, desktop effects and dozens of other resources straight to your desktop. We work with the new <a href="http://store.kde.org">KDE Store</a> to bring you a wide selection of addons ready to be installed.

### Desktop Search

{{<figure src="/announcements/plasma/5/5.12.0/krunner.png" alt="Desktop Search " class="text-center" width="600px" caption="Desktop Search">}}

Plasma lets you easily search your desktop for applications, folders, music, video, files... If you have it, Plasma can find it.

### Unified Look

{{<figure src="/announcements/plasma/5/5.12.0/gtk-integration.png" alt="Unified Look " class="text-center" width="600px" caption="Unified Look">}}

Plasma's default Breeze theme has a unified look across all the common programming toolkits - Qt 4 &amp; 5, GTK 2 &amp; 3, and LibreOffice.

### Phone Integration

{{<figure src="/announcements/plasma/5/5.12.0/kdeconnect.png" alt="Phone Integration " class="text-center" width="600px" caption="Phone Integration">}}

With KDE Connect, you can get notifications for new text messages on the desktop, easily transfer files between your computer and mobile device, silence music during calls, and even use your phone for remote control.

### Infinitely Customisable

{{<figure src="/announcements/plasma/5/5.12.0/customisable.png" alt="Infinitely Customisable " class="text-center" width="600px" caption="Infinitely Customisable">}}

Plasma is simple by default, but powerful when needed. You can customise it however you like with new widgets, panels, screens, and styles.
