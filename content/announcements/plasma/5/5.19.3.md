---
aliases:
- ../../plasma-5.19.3
changelog: 5.19.2-5.19.3
date: 2020-07-07
layout: plasma
video: true
asBugfix: true
---

+ KWin: Make sure tablet coordinates take decorations into account. <a href="https://commits.kde.org/kwin/311094ad8be4c76df2a4655fe71e7085fa9c4e14">Commit.</a> Fixes bug <a href="https://bugs.kde.org/423833">#423833</a>
+ Fix a KCM crash when no file manager is installed. <a href="https://commits.kde.org/plasma-desktop/74753128180a36c9fba154914b3a2384025c4893">Commit.</a> Fixes bug <a href="https://bugs.kde.org/422819">#422819</a>
+ Powerdevil: Fix compilation with Qt 5.15, this hit the time bomb too. <a href="https://commits.kde.org/powerdevil/0fa63b8685f82e5f626058dfc0f9461ae158599b">Commit.</a>
