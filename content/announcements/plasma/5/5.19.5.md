---
aliases:
- ../../plasma-5.19.5
changelog: 5.19.4-5.19.5
date: 2020-09-01
layout: plasma
video: true
asBugfix: true
---

+ Powerdevil: On wakeup from suspend restore remembered keyboard brightness. <a href="https://commits.kde.org/powerdevil/e1ed36480cec6a49c166347efba6ab52adc0c37c">Commit.</a>
+ KSysGuard: Correctly handle monitors list changing. <a href="https://commits.kde.org/ksysguard/dbb656d54fea47fcb322ffb8e54a3b59ade316fd">Commit.</a>
+ xdg-desktop-portal-kde: enable printing of multiple copies.
