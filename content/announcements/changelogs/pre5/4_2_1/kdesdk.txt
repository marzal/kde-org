------------------------------------------------------------------------
r914685 | woebbe | 2009-01-21 14:59:22 +0000 (Wed, 21 Jan 2009) | 1 line

bump version number
------------------------------------------------------------------------
r915128 | scripty | 2009-01-22 13:51:19 +0000 (Thu, 22 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r916785 | shaforo | 2009-01-26 00:27:22 +0000 (Mon, 26 Jan 2009) | 6 lines

workaround for Qt/X11 bug: if Del on NumPad is pressed, then pos is beyond end

BUG:181921
CCMAIL:kde-core-devel@kde.org


------------------------------------------------------------------------
r917136 | habacker | 2009-01-26 23:01:18 +0000 (Mon, 26 Jan 2009) | 1 line

backported crash fix 917128
------------------------------------------------------------------------
r917138 | habacker | 2009-01-26 23:05:27 +0000 (Mon, 26 Jan 2009) | 1 line

backported crash fix 917130
------------------------------------------------------------------------
r917288 | shaforo | 2009-01-27 15:00:15 +0000 (Tue, 27 Jan 2009) | 4 lines

this makes .po diffs include only useful info.
i'm sorry for not doing this earlier.


------------------------------------------------------------------------
r919506 | scripty | 2009-02-01 08:43:16 +0000 (Sun, 01 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r921735 | lueck | 2009-02-05 15:17:23 +0000 (Thu, 05 Feb 2009) | 1 line

fix wrong mailinglist address, backport from trunk
------------------------------------------------------------------------
r921832 | marchand | 2009-02-05 19:03:38 +0000 (Thu, 05 Feb 2009) | 1 line

backport kio_svn fixes from trunk
------------------------------------------------------------------------
r922554 | scripty | 2009-02-07 08:48:45 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r923537 | krake | 2009-02-08 22:12:26 +0000 (Sun, 08 Feb 2009) | 6 lines

Backport of r921041

Backport fixes made to a usage of this template in
playground/pim/googledata by Christophe Giboudeaux.


------------------------------------------------------------------------
r928150 | kkofler | 2009-02-19 00:17:30 +0000 (Thu, 19 Feb 2009) | 21 lines

Mass backport of Kompare bugfixes for KDE 4.2.1 (should not touch any translatable strings): backport revisions 913308, 913309, 922431 (Kompare only), 924527, 924539, 924540, 924880, 924959, 924960, 925266, 926028, 926131, 926143, 926225, 928082 (minus i18n string changes), 928141, 928146 from trunk.
CCBUG: 176804
CCBUG: 169692
CCBUG: 182792
CCBUG: 176797
CCBUG: 175251
CCBUG: 75794
CCBUG: 89781
CCBUG: 145956
CCBUG: 107489
CCBUG: 165421
CCBUG: 174924
CCBUG: 103651
CCBUG: 102800
CCMAIL: bruggie@gmail.com

Bump Kompare version from 3.5.2 to 3.5.3 (trunk is 4.0.0).

Tested on Fedora 9 i386, KDE 4.2.0.

Note: skipped revision 926198 because it introduces a string and #124121 is more a user error than a bug anyway.
------------------------------------------------------------------------
r928184 | kkofler | 2009-02-19 03:08:55 +0000 (Thu, 19 Feb 2009) | 2 lines

Fix build with GCC 4.4.
(backport revision 928183 from trunk)
------------------------------------------------------------------------
r930691 | kkofler | 2009-02-24 01:42:01 +0000 (Tue, 24 Feb 2009) | 2 lines

Remove some debug info that is not needed
backport revision 929602 by bruggie from trunk
------------------------------------------------------------------------
r930692 | kkofler | 2009-02-24 01:44:24 +0000 (Tue, 24 Feb 2009) | 1 line

Remove confusing always-disabled radiobuttons for RCS and Ed (part of trunk revision 929702).
------------------------------------------------------------------------
r930694 | kkofler | 2009-02-24 01:46:55 +0000 (Tue, 24 Feb 2009) | 2 lines

Stop creating dialogs on the heap when they are deleted again in the same function.
backport revision 929709 by bruggie from trunk
------------------------------------------------------------------------
r930695 | kkofler | 2009-02-24 01:51:09 +0000 (Tue, 24 Feb 2009) | 3 lines

Now the right help page is called for every tab in the preferences dialog. If this does not fix it then i dont know anymore :D Thanks for the bugreport.
backport revision 930135 by bruggie from trunk
CCBUG: 69081
------------------------------------------------------------------------
r930700 | kkofler | 2009-02-24 02:03:15 +0000 (Tue, 24 Feb 2009) | 27 lines

Many changes related to passing filename information from the part to
the modellist. The Kompare::Infostruct was already in place to update
everyone about changes but it was not used to communicate between the
part and model this commit fixes that. 

The real reason for this commit is that it fixes saving remote files. 
Do not give a local URL and pretend it is a remote URL, does not work. 
KIO::NetAccess::upload then simply does nothing because the file 
already exists.  

Refresh is fixed now. Refetch the files and dont reuse the local files
to determine the differences. Now that saving remote works it showed the
old result again. Some had already seen this, now it was easy to figure
out where it came from.

Fixed a silly copy and paste error where for the destination directory
source was used. Always fun these bugs.

Updated the caption in the titlebar to now have a " -- " to separate the
files so it is more clear which files/directories are compared. I read
somewhere in a bugreport people did not like this so fixed as well.

CCBUG:76904
CCBUG:81088
CCBUG:126870 Patch applied, second hunk not since this is not related

backport revision 930684 by bruggie from trunk
------------------------------------------------------------------------
r930701 | kkofler | 2009-02-24 02:05:38 +0000 (Tue, 24 Feb 2009) | 3 lines

Yes copy and paste error...
backport revision 930685 by bruggie from trunk
CCBUG: 139212
------------------------------------------------------------------------
r931116 | kkofler | 2009-02-24 20:12:27 +0000 (Tue, 24 Feb 2009) | 3 lines

Backport revisions 930812 and 931115 by bruggie from trunk (tested on Fedora 9, KDE 4.2.0):
Fix 2 FIXMEs regarding swapping source and destination. (Regression from the fix for remote files.)
No need to notify anyone about swapping source with destination. (Remove FIXMEs.)
------------------------------------------------------------------------
r931235 | kkofler | 2009-02-25 00:36:02 +0000 (Wed, 25 Feb 2009) | 3 lines

This fixes saving remotely when in comparingDirs mode.
Backport revision 931227 by bruggie from trunk.
No idea if this will make 4.2.1, it's late.
------------------------------------------------------------------------
r931540 | kkofler | 2009-02-25 13:05:01 +0000 (Wed, 25 Feb 2009) | 4 lines

Backport revisions 931164, 931184, 931210 by bruggie from trunk:
Fix leaking an entire directory copied from a remote location and some other small (TM) cleanups. (+ 2 followup fixes.)
Should make 4.2.1 as per Dirk's announcement.
CCMAIL: bruggie@gmail.com
------------------------------------------------------------------------
