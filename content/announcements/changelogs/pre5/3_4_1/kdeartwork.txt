2005-04-04 15:06 +0000 [r403136]  lunakl

	* kwin-styles/riscos/Makefile.am, kwin-styles/icewm/Makefile.am,
	  kwin-styles/system/Makefile.am: Backport #103208.

2005-04-30 03:55 +0000 [r408740]  brandybuck

	* styles/phase/phasestyle.h, styles/phase/phasestyle.cpp:
	  Backporting #103211 and 103519

2005-05-05 12:52 +0000 [r409718]  lunakl

	* kwin-styles/glow/glowclient.cpp,
	  kwin-styles/kstep/nextclient.cpp, kwin-styles/cde/cdeclient.cpp,
	  kwin-styles/kde1/kde1client.cpp,
	  kwin-styles/system/systemclient.cpp, kwin-styles/icewm/icewm.cpp,
	  kwin-styles/openlook/OpenLook.cpp,
	  kwin-styles/riscos/Manager.cpp: Backport #96079.

2005-05-08 09:07 +0000 [r410717]  binner

	* IconThemes/Makefile.am: fix for .svn/

2005-05-10 10:32 +0000 [r411876]  binner

	* kdeartwork.lsm: update lsm for release

2005-05-14 09:13 +0000 [r413649]  benb

	* debian/copyright, debian/kscreensaver.README.Debian (added),
	  debian/krotation.kss.1 (added),
	  debian/kscreensaver-xsavers.manpages (added), debian/control,
	  debian/README.Packaging (added), debian/kscreensaver.manpages,
	  debian/changelog, debian/kscreensaver-xsavers.install (added),
	  debian/kxsconfig.1, debian/rules,
	  debian/source.lintian-overrides, debian/kfiresaver.kss.1 (added),
	  debian/kscreensaver.install,
	  debian/kscreensaver-xsavers.README.Debian (added),
	  debian/kpendulum.kss.1 (added), debian/kswarm.kss.1,
	  debian/clean-sources (added): The packaging for this module in
	  the 3.4 branch is quite out of date. Bring it up to the current
	  state of packaging in debian unstable.

