------------------------------------------------------------------------
r1168080 | scripty | 2010-08-26 03:41:07 +0100 (dj, 26 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1167577 | weidendo | 2010-08-25 00:28:50 +0100 (dc, 25 ago 2010) | 3 lines

Backport of r1167565

Fix handling of Return in search widget of flat profile
------------------------------------------------------------------------
r1167576 | weidendo | 2010-08-25 00:28:40 +0100 (dc, 25 ago 2010) | 4 lines

Backport of r1167564: Fix bug 242900

Fix reading of very long source lines which screwed annotation.
Thanks to pto@linuxbog.dk and cratiport@gmail.com for noting and the fix.
------------------------------------------------------------------------
r1166069 | aacid | 2010-08-20 19:28:16 +0100 (dv, 20 ago 2010) | 4 lines

backport r1166068 | aacid | 2010-08-20 19:27:43 +0100 (Fri, 20 Aug 2010) | 4 lines

New tags to extract, patch by Bernard Massot

------------------------------------------------------------------------
r1159705 | mmrozowski | 2010-08-06 02:07:41 +0100 (dv, 06 ago 2010) | 1 line

Fix automagic antlr2 dependency.
------------------------------------------------------------------------
