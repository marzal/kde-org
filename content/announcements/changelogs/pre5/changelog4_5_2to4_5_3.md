---
aliases:
- ../changelog4_5_2to4_5_3
hidden: true
title: KDE 4.5.3 Changelog
---

<h2>Changes in KDE 4.5.3</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_5_3/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix support for big ZIP archives in KZip. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=216672">216672</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1187160&amp;view=rev">1187160</a>. </li>
      </ul>
      </div>
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix major">Avoid an infinite loop in KCategorizedView. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=226631">226631</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1181739&amp;view=rev">1181739</a>. </li>
        <li class="bugfix normal">Prevent that the wrong page in a KPageView is shown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249706">249706</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1185518&amp;view=rev">1185518</a>. </li>
        <li class="bugfix crash">Workaround an issue causing KSharedDataCache to mysteriously crash when changing the system time (including timezones for dual-boot users). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=253795">253795</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1187999&amp;view=rev">1187999</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_5_3/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">Dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Prevent that icons overlap in Details View when the zoom level is changed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=234600">234600</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1182798&amp;view=rev">1182798</a>. </li>
      </ul>
      </div>
      <h4><a name="plasma">plasma</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix minor">Nowplaying dataengine: do not get confused by applications exporting MPRIS2 interfaces. See SVN commit <a href="http://websvn.kde.org/?rev=1184471&amp;view=rev">1184471</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="4_5_3/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kshisen">kshisen</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Rebuild board when difficulty level has changed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230241">230241</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1182107&amp;view=rev">1182107</a>. </li>
        <li class="bugfix normal">Fix logical error in "unsolvable games" option. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=145953">145953</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1182109&amp;view=rev">1182109</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_5_3/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/marble" name="marble">Marble</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fixed handling of the zoom value. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249628">249628</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1185602&amp;view=rev">1185602</a>. </li>
        <li class="bugfix normal">Corrected broken painting of the atmosphere. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=254154">254154</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1185965&amp;view=rev">1185965</a>. </li>
      </ul>
      </div>
      <h4><a href="http://edu.kde.org/kgeography" name="kgeography">KGeography</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Update the name of a place in Azerbaijan. See SVN commit <a href="http://websvn.kde.org/?rev=1188209&amp;view=rev">1188209</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_5_3/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/ark/" name="ark">Ark</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not follow symlinks when creating tar archives. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=253059">253059</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1186377&amp;view=rev">1186377</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_5_3/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="libkcompactdisc">libkcompactdisc</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix compilation on DragonFly BSD. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=247643">247643</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1188014&amp;view=rev">1188014</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_5_3/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Save the visibility of menu bar and toolbar when closing as full screen. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=250370">250370</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1185956&amp;view=rev">1185956</a>. </li>
        <li class="bugfix ">XPS backend: support also application/oxps, which is the new MIME type name for XPS documents. See SVN commit <a href="http://websvn.kde.org/?rev=1187569&amp;view=rev">1187569</a>. </li>
        <li class="bugfix crash">DjVu backend: do not crash when handling links pointing to pages not in the document. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=254610">254610</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1189016&amp;view=rev">1189016</a>. </li>
      </ul>
      </div>
    </div>