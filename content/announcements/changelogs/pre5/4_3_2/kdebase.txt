------------------------------------------------------------------------
r1016702 | mueller | 2009-08-28 15:44:21 +0000 (Fri, 28 Aug 2009) | 2 lines

bump version numbers

------------------------------------------------------------------------
r1016720 | aseigo | 2009-08-28 16:45:31 +0000 (Fri, 28 Aug 2009) | 3 lines

use the app's corona, don't rely on the containment being in a good state
BUG:205504

------------------------------------------------------------------------
r1016727 | aseigo | 2009-08-28 17:39:09 +0000 (Fri, 28 Aug 2009) | 3 lines

backport of r1003860
CCBUG:198740

------------------------------------------------------------------------
r1016728 | aseigo | 2009-08-28 17:41:06 +0000 (Fri, 28 Aug 2009) | 3 lines

bacport r1003866
CCBUG:201761

------------------------------------------------------------------------
r1016750 | dfaure | 2009-08-28 19:27:28 +0000 (Fri, 28 Aug 2009) | 2 lines

Backport: hide old history module from sidebar. BUG 205521.

------------------------------------------------------------------------
r1016770 | darioandres | 2009-08-28 20:28:57 +0000 (Fri, 28 Aug 2009) | 3 lines

Backport: Adding akonadi_imap_resource


------------------------------------------------------------------------
r1016777 | darioandres | 2009-08-28 21:06:03 +0000 (Fri, 28 Aug 2009) | 5 lines

Backport changes:
Fix mapping of akonadi-imap
Added korgac mapping


------------------------------------------------------------------------
r1017002 | aseigo | 2009-08-29 14:31:21 +0000 (Sat, 29 Aug 2009) | 3 lines

proper parent check
CCBUG:193871

------------------------------------------------------------------------
r1017051 | dafre | 2009-08-29 16:49:34 +0000 (Sat, 29 Aug 2009) | 5 lines

BUG: 190862

Fix by backporting the change to 4.3


------------------------------------------------------------------------
r1017053 | aseigo | 2009-08-29 16:51:55 +0000 (Sat, 29 Aug 2009) | 2 lines

fix build

------------------------------------------------------------------------
r1017175 | ppenz | 2009-08-29 20:29:13 +0000 (Sat, 29 Aug 2009) | 5 lines

Backport of SVN commit 1017172:

- fixed issue that the horizontal progress bar and the space info bar get hidden sometimes when changing the setting

- use less vertical space for the capacity bar (thanks to Rafael Fernández López for the patch!)
------------------------------------------------------------------------
r1017218 | scripty | 2009-08-30 03:01:41 +0000 (Sun, 30 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1017576 | aseigo | 2009-08-30 22:46:10 +0000 (Sun, 30 Aug 2009) | 3 lines

fix causation of duplication
CCBUG:205619

------------------------------------------------------------------------
r1017603 | hindenburg | 2009-08-31 00:20:15 +0000 (Mon, 31 Aug 2009) | 5 lines

Correct KPart issue where the wrong context menu was used and invalid menus
were created.

CCBUG: 186745

------------------------------------------------------------------------
r1017604 | hindenburg | 2009-08-31 00:20:40 +0000 (Mon, 31 Aug 2009) | 1 line

update version
------------------------------------------------------------------------
r1017640 | scripty | 2009-08-31 03:00:15 +0000 (Mon, 31 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1017822 | stikonas | 2009-08-31 16:09:01 +0000 (Mon, 31 Aug 2009) | 3 lines

Fix pluralization support
BUG: 204846

------------------------------------------------------------------------
r1017881 | lueck | 2009-08-31 19:39:32 +0000 (Mon, 31 Aug 2009) | 1 line

change catalog name to match the name in the EXPORT macro
------------------------------------------------------------------------
r1018117 | aseigo | 2009-09-01 09:33:00 +0000 (Tue, 01 Sep 2009) | 3 lines

the text is set as html, so translate new lines into <br>s
CCBUG:204698

------------------------------------------------------------------------
r1018121 | jacopods | 2009-09-01 09:46:54 +0000 (Tue, 01 Sep 2009) | 2 lines

Backport performance improvements by aseigo

------------------------------------------------------------------------
r1018185 | aseigo | 2009-09-01 12:31:24 +0000 (Tue, 01 Sep 2009) | 3 lines

limit updates to 5/s
CCBUG:204491

------------------------------------------------------------------------
r1018401 | aseigo | 2009-09-01 17:01:27 +0000 (Tue, 01 Sep 2009) | 2 lines

backport engine performance rewrite

------------------------------------------------------------------------
r1018462 | stikonas | 2009-09-01 18:10:38 +0000 (Tue, 01 Sep 2009) | 3 lines

Fix syntax error in UI file.
BUG: 204846

------------------------------------------------------------------------
r1018514 | mlaurent | 2009-09-01 19:20:53 +0000 (Tue, 01 Sep 2009) | 2 lines

Backport: add icons

------------------------------------------------------------------------
r1018527 | aseigo | 2009-09-01 19:40:51 +0000 (Tue, 01 Sep 2009) | 2 lines

backport "only update status when the status changes" improvement

------------------------------------------------------------------------
r1018584 | aseigo | 2009-09-01 21:53:41 +0000 (Tue, 01 Sep 2009) | 2 lines

fixes

------------------------------------------------------------------------
r1018664 | pdamsten | 2009-09-02 03:08:58 +0000 (Wed, 02 Sep 2009) | 2 lines

Usage types are translated
CCBUG: 192970
------------------------------------------------------------------------
r1018666 | scripty | 2009-09-02 03:14:36 +0000 (Wed, 02 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1018671 | hindenburg | 2009-09-02 03:50:23 +0000 (Wed, 02 Sep 2009) | 4 lines

Ignore invalid parameters; can be the result of user doing cat < /dev/urandom.

CCBUG: 195795

------------------------------------------------------------------------
r1018673 | hindenburg | 2009-09-02 03:54:54 +0000 (Wed, 02 Sep 2009) | 4 lines

Temporary fix for crashing during Search when there is a match on top line.

CCBUG: 205495

------------------------------------------------------------------------
r1019042 | aseigo | 2009-09-02 16:56:30 +0000 (Wed, 02 Sep 2009) | 2 lines

backport Ruphy's potential-memory-leak fix

------------------------------------------------------------------------
r1019170 | scripty | 2009-09-03 04:09:24 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019183 | bcooksley | 2009-09-03 04:26:55 +0000 (Thu, 03 Sep 2009) | 2 lines

Backport commit 1008707.
CCBUG: 202119
------------------------------------------------------------------------
r1019186 | bcooksley | 2009-09-03 04:47:19 +0000 (Thu, 03 Sep 2009) | 3 lines

Backport commit r1019185.
Connect to correct signal so keyboard is a valid control device also.
CCBUG: 205830
------------------------------------------------------------------------
r1019343 | scheepmaker | 2009-09-03 11:08:35 +0000 (Thu, 03 Sep 2009) | 3 lines

Properly escape URL's in the job completed widgets. Backported from r1019337.


------------------------------------------------------------------------
r1019374 | scheepmaker | 2009-09-03 12:06:20 +0000 (Thu, 03 Sep 2009) | 4 lines

Make sure we always clean up completed jobs on plasma exit. They're temporary anyways.
Backported from r1019369


------------------------------------------------------------------------
r1019473 | scheepmaker | 2009-09-03 17:52:44 +0000 (Thu, 03 Sep 2009) | 4 lines

Let's just show the close button on notifications even on those with actions, because that works correctly now.
Backported from r1019471


------------------------------------------------------------------------
r1019488 | binner | 2009-09-03 18:17:22 +0000 (Thu, 03 Sep 2009) | 2 lines

fix Altra v Atra confusion

------------------------------------------------------------------------
r1019507 | lueck | 2009-09-03 18:53:56 +0000 (Thu, 03 Sep 2009) | 1 line

base doc backport for 4.3
------------------------------------------------------------------------
r1019525 | lueck | 2009-09-03 19:46:23 +0000 (Thu, 03 Sep 2009) | 1 line

base backport 2nd part for 4.3
------------------------------------------------------------------------
r1019621 | scripty | 2009-09-04 03:27:53 +0000 (Fri, 04 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019865 | dfaure | 2009-09-04 17:16:12 +0000 (Fri, 04 Sep 2009) | 3 lines

Tired of plasma crashing every 20-30 minutes for the last month or so, time to run it in valgrind.
First hit: init() calls expand() calls checkSettings() uses showToolTip(), not yet initialized in init() -> moving that up.

------------------------------------------------------------------------
r1019868 | dfaure | 2009-09-04 17:23:37 +0000 (Fri, 04 Sep 2009) | 2 lines

Fix use of uninitialized variables in AbstractTaskItem::setPreferredOffscreenSize()

------------------------------------------------------------------------
r1019889 | lueck | 2009-09-04 18:20:31 +0000 (Fri, 04 Sep 2009) | 1 line

add to backport for kdebase
------------------------------------------------------------------------
r1020017 | scripty | 2009-09-05 03:19:57 +0000 (Sat, 05 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1020312 | scripty | 2009-09-06 03:12:24 +0000 (Sun, 06 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1020661 | lueck | 2009-09-06 21:28:27 +0000 (Sun, 06 Sep 2009) | 1 line

change name in export macro to use the existing catalog
------------------------------------------------------------------------
r1020854 | dfaure | 2009-09-07 11:27:56 +0000 (Mon, 07 Sep 2009) | 3 lines

Optimization: don't write into a kconfig file every time we switch virtual desktops,
only when actually changing these settings.

------------------------------------------------------------------------
r1021007 | mart | 2009-09-07 20:39:46 +0000 (Mon, 07 Sep 2009) | 2 lines

cache the shadow, should be a lot faster, less QImage operation

------------------------------------------------------------------------
r1021168 | dfaure | 2009-09-08 12:53:50 +0000 (Tue, 08 Sep 2009) | 5 lines

Backport 1021167: remove part from m_mapViews before actually deleting it (which can
lead to a mouse-over-url event which is then sent to all views in the map).
Many thanks to lemma for the analysis of the problem.
(BUGS 200181, 184604)

------------------------------------------------------------------------
r1021212 | binner | 2009-09-08 14:40:19 +0000 (Tue, 08 Sep 2009) | 2 lines

backport spell fix

------------------------------------------------------------------------
r1021224 | darioandres | 2009-09-08 15:04:55 +0000 (Tue, 08 Sep 2009) | 5 lines

Backport to 4.3:
- Fix a small memory leak:
  * Call FcStrListDone to properly delete the FcStrList *list object


------------------------------------------------------------------------
r1021269 | freininghaus | 2009-09-08 17:54:03 +0000 (Tue, 08 Sep 2009) | 14 lines

Fix selection in the Details view in the following use case:

1. Ctrl-click item 1.
2. Enter the first letter of item 2, such that
   it will be selected and the new current item.
3. Shift-click item 3.

With this commit, all items between 2 and 3 will be selected, as
expected, and not all items between 1 and 3.

Fix will be in KDE 4.3.1.

CCBUG: 201459

------------------------------------------------------------------------
r1021316 | graesslin | 2009-09-08 20:38:09 +0000 (Tue, 08 Sep 2009) | 4 lines

Backport rev 1016998:
Don't crash when a window is closed while coverswitch is active
CCBUG: 184602

------------------------------------------------------------------------
r1021318 | graesslin | 2009-09-08 20:39:30 +0000 (Tue, 08 Sep 2009) | 4 lines

Backport rev 1020481:
Do not crash when a window get's closed while boxswitch is active.
CCBUG: 179865

------------------------------------------------------------------------
r1021386 | scripty | 2009-09-09 03:36:45 +0000 (Wed, 09 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1021617 | lunakl | 2009-09-09 16:57:05 +0000 (Wed, 09 Sep 2009) | 5 lines

Backport:
First, commandURL means the URL itself is enoug.
Second, we are not that .de-centric to use google.de.


------------------------------------------------------------------------
r1021689 | darioandres | 2009-09-09 20:07:54 +0000 (Wed, 09 Sep 2009) | 3 lines

- Fix a small memory leak


------------------------------------------------------------------------
r1021700 | darioandres | 2009-09-09 20:46:22 +0000 (Wed, 09 Sep 2009) | 5 lines

Backport to 4.3:
- Do not use a pointer for the UI object
  - Fixes a small memory leak


------------------------------------------------------------------------
r1021721 | darioandres | 2009-09-09 21:46:28 +0000 (Wed, 09 Sep 2009) | 4 lines

Backport to 4.3:
- Fix some memory leaks using QObject parenting


------------------------------------------------------------------------
r1021730 | darioandres | 2009-09-09 22:09:24 +0000 (Wed, 09 Sep 2009) | 3 lines

- Fix some memory leaks


------------------------------------------------------------------------
r1021745 | darioandres | 2009-09-09 22:40:23 +0000 (Wed, 09 Sep 2009) | 4 lines

Backport to 4.3:
- Fix a memory leak when removing a row/item


------------------------------------------------------------------------
r1021782 | darioandres | 2009-09-10 01:23:30 +0000 (Thu, 10 Sep 2009) | 8 lines

Backport to 4.3:
- Create the UI objects on stack to don't have to delete the pointer later
  (and it is the preferred method anyways)
- Delete the SearchProvider objects on destruction or removal

* This two changes fix several memory leaks


------------------------------------------------------------------------
r1022002 | jacopods | 2009-09-10 14:27:06 +0000 (Thu, 10 Sep 2009) | 5 lines

KTraderParse is not thread safe in 4.3 - welcome back bigLock()
CCBUG: 192536
CCMAIL: pino@kde.org


------------------------------------------------------------------------
r1022012 | jacopods | 2009-09-10 14:49:42 +0000 (Thu, 10 Sep 2009) | 2 lines

use the bigLock more efficiently - thanks Pino

------------------------------------------------------------------------
r1022197 | darioandres | 2009-09-11 02:47:30 +0000 (Fri, 11 Sep 2009) | 6 lines

Backport to 4.3:
- Create the UI (designer file) object on stack:
  - It is the preferred way
  - We don't have to delete the dummy pointer at the end


------------------------------------------------------------------------
r1022486 | craig | 2009-09-11 20:48:11 +0000 (Fri, 11 Sep 2009) | 2 lines

Fix memory leaks as reported at http://reviewboard.kde.org/r/1567/

------------------------------------------------------------------------
r1022565 | scripty | 2009-09-12 03:05:01 +0000 (Sat, 12 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022818 | ossi | 2009-09-13 09:01:14 +0000 (Sun, 13 Sep 2009) | 7 lines

implement scalemode attribute of pixmap/svg elements

while this is a (small) feature, it is needed to fix bug 169772
properly. i think it had enough testing in trunk - see r1009279,
r1011736 & r1011925.


------------------------------------------------------------------------
r1022821 | ossi | 2009-09-13 09:10:31 +0000 (Sun, 13 Sep 2009) | 6 lines

fix the themes' background images' aspect ratio

backport of r1009280 & r1011924

CCBUG: 169772

------------------------------------------------------------------------
r1022979 | mart | 2009-09-13 16:51:34 +0000 (Sun, 13 Sep 2009) | 4 lines

since te framesvg of the task backgrounds is shared, sometimes we have
to resize it even if there wasn't a resizeevent (due to popup tasks
smaller than the tasks in the taskbar for instance)

------------------------------------------------------------------------
r1023072 | markuss | 2009-09-13 20:25:43 +0000 (Sun, 13 Sep 2009) | 1 line

BUG: 207136
------------------------------------------------------------------------
r1023164 | scripty | 2009-09-14 03:13:22 +0000 (Mon, 14 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1023325 | mzanetti | 2009-09-14 14:25:17 +0000 (Mon, 14 Sep 2009) | 2 lines

backport of commit 1023324

------------------------------------------------------------------------
r1023532 | jacopods | 2009-09-14 21:38:21 +0000 (Mon, 14 Sep 2009) | 4 lines

Apply the "remove-then-delete" cure also to configuration widgets and button
This also works around a quite obscure qt bug dealing with caching. 


------------------------------------------------------------------------
r1023553 | aacid | 2009-09-14 22:23:06 +0000 (Mon, 14 Sep 2009) | 3 lines

fix the catalog name to be the correct one
CCMAIL: aseigo@kde.org

------------------------------------------------------------------------
r1023621 | scripty | 2009-09-15 03:11:35 +0000 (Tue, 15 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1023690 | dfaure | 2009-09-15 09:09:57 +0000 (Tue, 15 Sep 2009) | 2 lines

Backport 1023320 and 1023611, to fix 167388 (trashrc not reparsed so "Empty" wasn't set to false again)

------------------------------------------------------------------------
r1023944 | mart | 2009-09-15 16:41:34 +0000 (Tue, 15 Sep 2009) | 2 lines

backport highlight effect fix 

------------------------------------------------------------------------
r1023950 | mart | 2009-09-15 17:08:15 +0000 (Tue, 15 Sep 2009) | 2 lines

chack if we are the root item

------------------------------------------------------------------------
r1024026 | sune | 2009-09-15 20:35:46 +0000 (Tue, 15 Sep 2009) | 1 line

make mime selection handling better, backport of 1024025.
------------------------------------------------------------------------
r1024101 | darioandres | 2009-09-16 02:11:32 +0000 (Wed, 16 Sep 2009) | 3 lines

- Add kpackagekitsmarticon to mappings file


------------------------------------------------------------------------
r1024109 | darioandres | 2009-09-16 02:58:08 +0000 (Wed, 16 Sep 2009) | 10 lines

Backport to 4.3:
SVN commit 1024108 by darioandres:

- Do not delete the widgetItem before deleting the file, we are going to cause
a crash.
  - Delete the widgetItem as the last step

CCBUG: 207240


------------------------------------------------------------------------
r1024111 | scripty | 2009-09-16 03:14:53 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024305 | dfaure | 2009-09-16 11:05:06 +0000 (Wed, 16 Sep 2009) | 3 lines

Don't let Enter in the mimetype filter lineedit close the dialog
CCBUG: 66969

------------------------------------------------------------------------
r1024317 | jacopods | 2009-09-16 11:24:09 +0000 (Wed, 16 Sep 2009) | 3 lines

backport r996370: rearrange windows if a window is closed even when fade effect is enabled 
CCBUG:200084

------------------------------------------------------------------------
r1024438 | scripty | 2009-09-16 16:10:46 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024528 | darioandres | 2009-09-16 19:11:33 +0000 (Wed, 16 Sep 2009) | 8 lines

Backport to 4.3:
SVN commit 1024527 by darioandres:

- Show a better (and bold) message about an unsuccessful BKO login

CCBUG: 207516


------------------------------------------------------------------------
r1024530 | darioandres | 2009-09-16 19:15:20 +0000 (Wed, 16 Sep 2009) | 4 lines

Stupid me, backporting string changes is not allowed. Reverting
CCBUG: 207516


------------------------------------------------------------------------
r1024614 | segato | 2009-09-16 22:29:37 +0000 (Wed, 16 Sep 2009) | 4 lines

backport r1024608
on windows remove the / from the beginning of the path, toLocalFile returns an empty string because the 
zip/tar/ar protocols aren't recognized as local

------------------------------------------------------------------------
r1024835 | mlaurent | 2009-09-17 10:14:13 +0000 (Thu, 17 Sep 2009) | 3 lines

Backport:
wordwrap text

------------------------------------------------------------------------
r1024912 | jacopods | 2009-09-17 14:57:56 +0000 (Thu, 17 Sep 2009) | 3 lines

bigLock is gone again; KServiceTrader should be now thread-safe also in branch (thanks pinotree)
CCBUG: 192536

------------------------------------------------------------------------
r1025049 | dfaure | 2009-09-17 21:56:33 +0000 (Thu, 17 Sep 2009) | 2 lines

backport: allow a combo with 1 item for L3thal on irc.

------------------------------------------------------------------------
r1025054 | dfaure | 2009-09-17 22:10:30 +0000 (Thu, 17 Sep 2009) | 2 lines

Fix docu for --combobox, it doesn't take tags (and didn't in kde3 either)

------------------------------------------------------------------------
r1025329 | pdamsten | 2009-09-18 12:19:00 +0000 (Fri, 18 Sep 2009) | 2 lines

Backport 'Make sure interfaces are always shown in the same order.'
CCBUG: 200551
------------------------------------------------------------------------
r1025419 | dfaure | 2009-09-18 18:00:25 +0000 (Fri, 18 Sep 2009) | 3 lines

Update the controller's url upon redirection, otherwise pasting would paste into the old url.
BUG: 186947

------------------------------------------------------------------------
r1025548 | scripty | 2009-09-19 03:01:03 +0000 (Sat, 19 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025772 | darioandres | 2009-09-19 16:39:05 +0000 (Sat, 19 Sep 2009) | 4 lines

- Backport:
  - Add kscreenlocker to the mappings file


------------------------------------------------------------------------
r1026202 | scripty | 2009-09-21 03:00:11 +0000 (Mon, 21 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1026290 | dfaure | 2009-09-21 11:12:11 +0000 (Mon, 21 Sep 2009) | 5 lines

Backport for KDE-4.3.2, and close the master bug:
The redirection from zip to file wasn't appearing in konqueror because
zip:/tmp/ != zip:/tmp => do comparison without trailing slash.
BUG: 79302

------------------------------------------------------------------------
r1026784 | lueck | 2009-09-22 15:53:51 +0000 (Tue, 22 Sep 2009) | 2 lines

backport from trunk r1026782: add missing files for message extraction
CCMAIL:kde-18n-doc@kde.org
------------------------------------------------------------------------
r1026973 | scripty | 2009-09-23 03:15:57 +0000 (Wed, 23 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1027000 | camuffo | 2009-09-23 08:52:12 +0000 (Wed, 23 Sep 2009) | 6 lines

backport SVN commit r1026902

don't resize the panel on themeUpdated()

BUG: 184905

------------------------------------------------------------------------
r1027050 | trueg | 2009-09-23 09:48:37 +0000 (Wed, 23 Sep 2009) | 1 line

backport: use correct icons
------------------------------------------------------------------------
r1027306 | graesslin | 2009-09-23 19:54:03 +0000 (Wed, 23 Sep 2009) | 3 lines

Backport r1024275: Adding a safety check in coverswitch so that the input window is not destroyed twice.
CCBUG: 207554

------------------------------------------------------------------------
r1027313 | graesslin | 2009-09-23 20:16:54 +0000 (Wed, 23 Sep 2009) | 3 lines

Backport r1022489: Remove flicker when changing effect plugins by not fully reloading compositing plugins anymore.
CCBUG: 183107

------------------------------------------------------------------------
r1027407 | scripty | 2009-09-24 03:14:01 +0000 (Thu, 24 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1027429 | beatwolf | 2009-09-24 07:45:43 +0000 (Thu, 24 Sep 2009) | 3 lines

correct ksmserver interaction of the wallpaper, this is the backport
CCBUG: 206788

------------------------------------------------------------------------
r1027747 | alexmerry | 2009-09-24 17:00:24 +0000 (Thu, 24 Sep 2009) | 6 lines

Backport r1027743: Make sure we don't add a window task that has been scheduled for deletion back
into the layout.

CCBUG: 199325


------------------------------------------------------------------------
r1027900 | scripty | 2009-09-25 03:14:41 +0000 (Fri, 25 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1028079 | camuffo | 2009-09-25 19:36:46 +0000 (Fri, 25 Sep 2009) | 4 lines

backport: set unmount true for optical discs

CCBUG: 199652

------------------------------------------------------------------------
r1028162 | hindenburg | 2009-09-26 02:32:47 +0000 (Sat, 26 Sep 2009) | 4 lines

On the command line expand ./ for -e only.

CCBUG: 202302

------------------------------------------------------------------------
r1028163 | hindenburg | 2009-09-26 02:36:15 +0000 (Sat, 26 Sep 2009) | 1 line

Update version
------------------------------------------------------------------------
r1028290 | darioandres | 2009-09-26 14:01:26 +0000 (Sat, 26 Sep 2009) | 3 lines

- Add printer-applet-kde to mappings file


------------------------------------------------------------------------
r1028737 | scripty | 2009-09-28 03:05:15 +0000 (Mon, 28 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1029316 | helio | 2009-09-29 14:04:35 +0000 (Tue, 29 Sep 2009) | 1 line

- Backport custom target build dependency instead of macro to allow cmake > 2.6 works
------------------------------------------------------------------------
r1029743 | lunakl | 2009-09-30 16:38:03 +0000 (Wed, 30 Sep 2009) | 6 lines

Backport r1029742.
Do not force disabling of imported actions. Some, such as PrintScreen
handling, should be enabled by default.
BUG: 166608


------------------------------------------------------------------------
r1029774 | mdutkiewicz | 2009-09-30 17:44:47 +0000 (Wed, 30 Sep 2009) | 1 line

backport of r1029768
------------------------------------------------------------------------
r1029920 | scripty | 2009-10-01 02:46:23 +0000 (Thu, 01 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1030003 | dfaure | 2009-10-01 11:24:11 +0000 (Thu, 01 Oct 2009) | 4 lines

Ensure the document of the elements (during dnd or pasting) stays alive as long as the elements, otherwise we get crashes.
This commit needs an updated kdelibs. Fix will be in KDE-4.3.2.
BUG: 160679

------------------------------------------------------------------------
r1030059 | darioandres | 2009-10-01 13:16:32 +0000 (Thu, 01 Oct 2009) | 14 lines

Backport to 4.3 of:
SVN commit 1030054 by darioandres:

- Until we redesign the logic to avoid a bad re-entrancy caused by
processEvents while searching
  into file contents, try to not crash at least. (metaKeyRx could be already
deleted)

- Fix a memleak

CCBUG: 193560
CCBUG: 203325


------------------------------------------------------------------------
r1030230 | cfeck | 2009-10-01 23:01:19 +0000 (Thu, 01 Oct 2009) | 4 lines

Fix saving terminal application config (backport r1030229)

CCBUG: 209044

------------------------------------------------------------------------
r1030277 | scripty | 2009-10-02 02:56:46 +0000 (Fri, 02 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
