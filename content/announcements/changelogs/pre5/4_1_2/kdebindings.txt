------------------------------------------------------------------------
r856418 | sedwards | 2008-09-02 22:25:27 +0200 (Tue, 02 Sep 2008) | 3 lines

Better support for gettings at the different interfaces used in Kate.


------------------------------------------------------------------------
r864066 | rdale | 2008-09-23 22:51:48 +0200 (Tue, 23 Sep 2008) | 3 lines

* Promote the ruby bindings from the trunk to the KDE 4.1 release branch


------------------------------------------------------------------------
r864068 | rdale | 2008-09-23 22:54:01 +0200 (Tue, 23 Sep 2008) | 3 lines

* Promote the kalyptus bindings code generator from the trunk to the KDE 4.1 release 
branch

------------------------------------------------------------------------
r864087 | rdale | 2008-09-23 23:51:31 +0200 (Tue, 23 Sep 2008) | 3 lines

* Promote the cmake file from the trunk to the KDE 4.1 release branch


------------------------------------------------------------------------
r864089 | rdale | 2008-09-23 23:53:08 +0200 (Tue, 23 Sep 2008) | 4 lines

* Promote the C# bindings from the trunk to the KDE 4.1 release branch
* Regenerate the KDE and Plasma sources from the 4.1 headers


------------------------------------------------------------------------
r864241 | rdale | 2008-09-24 11:52:20 +0200 (Wed, 24 Sep 2008) | 3 lines

* Fix a few issues with kdebindings for KDE 4.1.2 and make it build


------------------------------------------------------------------------
