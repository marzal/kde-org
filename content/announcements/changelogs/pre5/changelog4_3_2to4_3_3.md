---
aliases:
- ../changelog4_3_2to4_3_3
hidden: true
title: KDE 4.3.3 Changelog
---

<h2>Changes in KDE 4.3.3</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_3_3/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make sure we return the names of the correct translators in KAboutData::translators(). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=210530">210530</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1037255&amp;view=rev">1037255</a>. </li>
        <li class="bugfix ">Ensure that *.doc files are recognized as msword files over remote protocols (smb, sftp, fish) as well. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204139">204139</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032366&amp;view=rev">1032366</a>. </li>
        <li class="bugfix ">Fix crash during concurrent access to KLocale's catalogs from multiple threads (take 2). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=209712">209712</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032185&amp;view=rev">1032185</a>. </li>
        <li class="bugfix ">Fix invokeBrowser for the case where the configured webbrowser is a command with multiple arguments (kde-4.3.2 regression). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=210529">210529</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1035593&amp;view=rev">1035593</a>. </li>
      </ul>
      </div>
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Prevent conflict between New tab and Remove Tab on middle-click of mouse button in Konqueror. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=188587">188587</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1039102&amp;view=rev">1039102</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Honor the confirm overwrite flag in KFileDialog when called from Qt applications. See SVN commit <a href="http://websvn.kde.org/?rev=1035728&amp;view=rev">1035728</a>. </li>
        <li class="bugfix ">Fix file loss while moving a directory, when some files cannot be moved and are skipped (e.g. on disk full). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=208418">208418</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1031746&amp;view=rev">1031746</a>. </li>
        <li class="bugfix ">Remove error message shown when the user types a HTTP url in the file dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=197945">197945</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1035523&amp;view=rev">1035523</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Copy (Ctrl+C) in frames. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=187403">187403</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032495&amp;view=rev">1032495</a>. </li>
        <li class="bugfix ">Fix crash when closing tab with the find bar open. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=207173">207173</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1028036&amp;view=rev">1028036</a>. </li>
        <li class="bugfix ">Allow to focus readonly lineedits and textedits and to copy from them. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=190188">190188</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032808&amp;view=rev">1032808</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_3_3/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix problem with multiple rubberband selections using the Control key: Don't clear the previous selection when the view is scrolled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=190703">190703</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1040024&amp;view=rev">1040024</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix issues with detached tabs: Don't crash when opening a new tab in the detached window, and make sure that the current view profile is loaded for the detached window. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=203069">203069</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=210686">210686</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1036044&amp;view=rev">1036044</a>. </li>
        <li class="bugfix ">Fix crash when adding tab in a window created from a detached tab. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203069">203069</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1036044&amp;view=rev">1036044</a>. </li>
      </ul>
      </div>
      <h4><a name="keditbookmarks">keditbookmarks</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix wrong behavior during DnD of bookmarks. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160679">160679</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032328&amp;view=rev">1032328</a>. </li>
      </ul>
      </div>
      <h4><a name="kwin">kwin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Speed up window resizing after a window reaches its minimum size. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183263">183263</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030474&amp;view=rev">1030474</a>. </li>
        <li class="bugfix ">Prevent jerkiness of the first two frames of the logout blur effect. See SVN commit <a href="http://websvn.kde.org/?rev=1030720&amp;view=rev">1030720</a>. </li>
        <li class="bugfix ">Prevent KWin from changing window opacities by itself. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=209274">209274</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030835&amp;view=rev">1030835</a>. </li>
        <li class="bugfix ">Fix moving of fullscreen windows between Xinerama screens. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=188827">188827</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030846&amp;view=rev">1030846</a>. </li>
        <li class="bugfix ">Speed up window resizing when using the rubber band mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=181800">181800</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030922&amp;view=rev">1030922</a>. </li>
        <li class="bugfix ">Detect when programs change the no border hint in _MOTIF_WM_HINTS. Used by Chromium. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=201523">201523</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030933&amp;view=rev">1030933</a>. </li>
        <li class="bugfix ">Select the correct desktop in the desktop grid effect when the user doesn't move the mouse before clicking. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=207271">207271</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030941&amp;view=rev">1030941</a>. </li>
        <li class="bugfix ">Don't trigger the slideback effect when closing a window and the newly focused window isn't on the top of the stacking order. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=196900">196900</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1031355&amp;view=rev">1031355</a>. </li>
        <li class="bugfix ">Fix the invert effect. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=212339">212339</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1042535&amp;view=rev">1042535</a>. </li>
      </ul>
      </div>
      <h4><a name="l10n">l10n</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix the description of the georgian locale. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=190431">190431</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030882&amp;view=rev">1030882</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeplasma-addons"><a name="kdeplasma-addons">kdeplasma-addons</a><span class="allsvnchanges"> [ <a href="4_3_3/kdeplasma-addons.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="microblog applet">Microblog Applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Twitter now shows posts with friends and shows latest post. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=210176">210176</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1033679&amp;view=rev">1033679</a>. </li>
      </ul>
      </div>
      <h4><a name="rememberthemilk applet">RememberTheMilk Applet</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Use https:// instead of http://. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202826">202826</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1034000&amp;view=rev">1034000</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_3_3/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/kgeography" name="kgeography">kgeography</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix switch between Mexico state and the federal district. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=209602">209602</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032032&amp;view=rev">1032032</a>. </li>
      </ul>
      </div>
      <h4><a href="http://edu.kde.org/klettres" name="klettres">klettres</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix backspace key. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=209821">209821</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032767&amp;view=rev">1032767</a>. </li>
      </ul>
      </div>
      <h4><a href="http://edu.kde.org/kstars" name="kstars">kstars</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixing the calculation of the illuminated fraction of the moon. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=209646">209646</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1032036&amp;view=rev">1032036</a>. </li>
        <li class="bugfix ">Fix incorrectly displayed parameters of objects in details dialog. See SVN commit <a href="http://websvn.kde.org/?rev=1032051&amp;view=rev">1032051</a>. </li>
        <li class="bugfix ">Using the keyboard to select a link in details dialog now works. See SVN commit <a href="http://websvn.kde.org/?rev=1039909&amp;view=rev">1039909</a>. </li>
        <li class="bugfix ">Fix issues with links in the details dialog in translated versions. See SVN commit <a href="http://websvn.kde.org/?rev=1039932&amp;view=rev">1039932</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="4_3_3/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://games.kde.org/game.php?game=kpat" name="kpat">kpat</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix a layouting problem with KPat and RTL languages. See SVN commit <a href="http://websvn.kde.org/?rev=1037762&amp;view=rev">1037762</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_3_3/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Make margin cropping work on landscape pages. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=196761">196761</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1039156&amp;view=rev">1039156</a>. </li>
        <li class="bugfix ">Make the PS renderer not crash when opening several files at once. See SVN commit <a href="http://websvn.kde.org/?rev=1039192&amp;view=rev">1039192</a>. </li>
        <li class="bugfix ">Fix problem rendering manually rotated pages. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=211467">211467</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1039571&amp;view=rev">1039571</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_3_3/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Disable 'New Alarm from Template' option when no alarm templates exist. See SVN commit <a href="http://websvn.kde.org/?rev=1041425&amp;view=rev">1041425</a>. </li>
          <li class="improvement">Improve error feedback when choosing a file to display. See SVN commit <a href="http://websvn.kde.org/?rev=1036844&amp;view=rev">1036844</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix a translation problem in KAlarm. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=209401">209401</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1031368&amp;view=rev">1031368</a>. </li>
        <li class="bugfix ">Interpret '~' (i.e. home directory) properly in entered file names. See SVN commit <a href="http://websvn.kde.org/?rev=1037271&amp;view=rev">1037271</a>. </li>
        <li class="bugfix crash">Fix crash when calendar formats are updated at login, during session restoration. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=210552">210552</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1035835&amp;view=rev">1035835</a>. </li>
        <li class="bugfix ">Update date-only alarm trigger times when user changes the start-of-day time. See SVN commit <a href="http://websvn.kde.org/?rev=1032499&amp;view=rev">1032499</a>. </li>
        <li class="bugfix ">Fix recurring date-only alarm triggering repeatedly and eating up CPU (depending on start of day time and time zone). See SVN commit <a href="http://websvn.kde.org/?rev=1032418&amp;view=rev">1032418</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_3_3/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">On first startup the columns in keymanager now have useful default widths. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=192375">192375</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1030866&amp;view=rev">1030866</a>. </li>
        <li class="bugfix ">When selecting keys to encrypt to make "select default key" shortcut work. See SVN commit <a href="http://websvn.kde.org/?rev=1040119&amp;view=rev">1040119</a>. </li>
        <li class="bugfix crash">Work around crash on startup. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=203497">203497</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1040127&amp;view=rev">1040127</a>. </li>
        <li class="bugfix ">Properly handle non-ascii characters when creating or modifying keys. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=211399">211399</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1040201&amp;view=rev">1040201</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/okteta" name="okteta">Okteta</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Don't crash if inserting from empty clipboard. See SVN commit <a href="http://websvn.kde.org/?rev=1036158&amp;view=rev">1036158</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_3_3/kdemultimedia.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix a possible crash on startup due to the /Player DBus object being created too early. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=211755">211755</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1042067&amp;view=rev">1042067</a>. </li>
        <li class="bugfix ">Fix a possible crash on shutdown due to the CoverManager being shutdown too early. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=192371">192371</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1042068&amp;view=rev">1042068</a>. </li>
        <li class="bugfix ">(Possibly) fix JuK skipping every other track on some Phonon backends.
          Patch provided by Kåre Särs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=204391">204391</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1042537&amp;view=rev">1042537</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="4_3_3/kdesdk.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kompare">Kompare</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix a 4.3.2 regression in diff parsing in case there are more deletions than insertions in a hunk (due to a typo in a bugfix). See SVN commit <a href="http://websvn.kde.org/?rev=1033955&amp;view=rev">1033955</a>. </li>
      </ul>
      </div>
    </div>