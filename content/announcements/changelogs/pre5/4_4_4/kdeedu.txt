------------------------------------------------------------------------
r1121279 | scripty | 2010-05-01 13:52:35 +1200 (Sat, 01 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1122801 | nienhueser | 2010-05-05 06:30:24 +1200 (Wed, 05 May 2010) | 1 line

fix missing member initialization. Backport of commit 1071915.
------------------------------------------------------------------------
r1124899 | dfaure | 2010-05-10 21:00:21 +1200 (Mon, 10 May 2010) | 2 lines

Fix kbuildsycoca warning

------------------------------------------------------------------------
r1125543 | nienhueser | 2010-05-12 05:25:22 +1200 (Wed, 12 May 2010) | 5 lines

Plugins are shared among QPluginLoader instances. The root components must not be deleted in the PluginManager dtor, otherwise dangling pointers appear in other PluginManager instances (leads to double deletion attempts). This affects at least applications using more than one MarbleWidget (e.g. digikam).
CCBUG: 235238
Backport of commit 1123153.
CCBUG: 234311

------------------------------------------------------------------------
