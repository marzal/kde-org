------------------------------------------------------------------------
r1230072 | rkcosta | 2011-05-02 14:58:48 +1200 (Mon, 02 May 2011) | 2 lines

Add some TODO notes for the future me.

------------------------------------------------------------------------
r1230073 | rkcosta | 2011-05-02 14:59:37 +1200 (Mon, 02 May 2011) | 6 lines

Do not fallback to kioslaves on drag'n'drop.

Using kioslaves was confusing and did not make much sense, as we end
up using Ark to bypass Ark.


------------------------------------------------------------------------
r1230074 | rkcosta | 2011-05-02 15:00:10 +1200 (Mon, 02 May 2011) | 2 lines

Remove some spaces within parenthesis mlaurent introduced a while ago.

------------------------------------------------------------------------
r1230208 | scripty | 2011-05-03 02:13:07 +1200 (Tue, 03 May 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1230711 | dakon | 2011-05-07 02:57:59 +1200 (Sat, 07 May 2011) | 6 lines

show "Configure KGpg" as last entry in settings menu of editor

BUG:272622

Backport of r1230709

------------------------------------------------------------------------
r1230915 | rkcosta | 2011-05-09 05:28:19 +1200 (Mon, 09 May 2011) | 13 lines

Put the libarchive detection calls before configure_file().

The generated config.h was using some variables defined only after
plugins/CMakeLists.txt called macro_optional_find_package(LibArchive),
however the top-level CMakeLists.txt relied on these variables already
being set, which was never the case.

This should fix the case in which a proper libarchive version was
present but Ark could not create tar.{xz,lzma} archives.

BUG: 271553
FIXED-IN: 4.6.4

------------------------------------------------------------------------
r1231023 | scripty | 2011-05-10 03:10:47 +1200 (Tue, 10 May 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1231302 | rkcosta | 2011-05-11 06:56:21 +1200 (Wed, 11 May 2011) | 8 lines

AddDialog: Confirm if the user wants to overwrite an existing file.

So far, if the user created an archive via "Compress To" (or any other way that
used AddDialog), an existing file with the same name would be overwritten
without any confirmation.

CCBUG: 272914

------------------------------------------------------------------------
r1231313 | rkcosta | 2011-05-11 09:35:54 +1200 (Wed, 11 May 2011) | 12 lines

AddDialog: Add a default file extension to the file being created.

KFileDialog (actually, KFileWidget)'s does not append an extension to the
selected file when it already exists, which is always the case when a single
file is being compressed, as the default file name is the file's own name.

To work around this, we always add the main extension for the current mimetype
the file passed to setSelection().

BUG: 272914
FIXED-IN: 4.6.4

------------------------------------------------------------------------
r1231432 | kossebau | 2011-05-12 08:17:30 +1200 (Thu, 12 May 2011) | 1 line

update version string
------------------------------------------------------------------------
r1231642 | rkcosta | 2011-05-13 08:28:11 +1200 (Fri, 13 May 2011) | 6 lines

Add widgets to QSplitter with addWidget().

No functional changes, but the documentation says it is good practice to add
child widgets with {add,insert}Widget instead of relying on childEvent()
adding.

------------------------------------------------------------------------
r1231646 | rkcosta | 2011-05-13 09:22:29 +1200 (Fri, 13 May 2011) | 12 lines

Do not pass widget(), which is a QSplitter, as a parent to KFileDialog.

Oh, we all love nested event loops. A QSplitter is passed as a parent to
KFileDialog, which creates a KFileWidget. KFileWidget runs a nested event loop
to stat the specified directory, which leads to posted events being processed,
including QSplitter::childEvent(). childEvent() calls KFileDialog::show(),
which shows the KFileDialog in a non-modal state. When KFileDialog::exec() is
called, the dialog is already shown and is non-modal.

BUG: 264819
FIXED-IN: 4.6.4

------------------------------------------------------------------------
r1231724 | scripty | 2011-05-14 02:18:36 +1200 (Sat, 14 May 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1231924 | rkcosta | 2011-05-15 11:00:48 +1200 (Sun, 15 May 2011) | 5 lines

Optimize ArchiveModel::cleanFileName a little.

We do not need to create another QString for the operations we are
going to perform.

------------------------------------------------------------------------
r1231925 | rkcosta | 2011-05-15 11:01:56 +1200 (Sun, 15 May 2011) | 5 lines

Use qStableSort instead of qSort.

If the items are considered equal in comparison, leave them in the same
order.

------------------------------------------------------------------------
r1231926 | rkcosta | 2011-05-15 11:02:35 +1200 (Sun, 15 May 2011) | 5 lines

Always sort folders before files.

BUG: 234373
FIXED-IN: 4.6.4

------------------------------------------------------------------------
r1233138 | rkcosta | 2011-05-23 16:22:27 +1200 (Mon, 23 May 2011) | 5 lines

batchextract: Remember to call the parent constructor.

For some unknown reason, BatchExtract's constructor was not calling
KCompositeJob's constructor.

------------------------------------------------------------------------
r1233570 | rkcosta | 2011-05-26 15:58:09 +1200 (Thu, 26 May 2011) | 7 lines

Split clizipplugin into header and implementation.

It is much cleaner to read now, which should help with the upcoming
bug fixes.

No functional changes introduced.

------------------------------------------------------------------------
r1233571 | rkcosta | 2011-05-26 15:59:22 +1200 (Thu, 26 May 2011) | 10 lines

Make CliInterface::escapeFileName() more general.

Instead of always escaping some characters with backslashes,
CliInterface::escapeFileName() is now a virtual method which should be
reimplemented by any plugin which needs to, and the default
implementation just returns an unchanged QString.

For now, only clizipplugin reimplements it by prefixing some
characters with backslashes.

------------------------------------------------------------------------
r1233572 | rkcosta | 2011-05-26 16:00:04 +1200 (Thu, 26 May 2011) | 4 lines

Make the QRegExp object const, remove unused variable.

SVN_SILENT

------------------------------------------------------------------------
r1233573 | rkcosta | 2011-05-26 16:00:57 +1200 (Thu, 26 May 2011) | 9 lines

Remove trailing '/'s from directories passed to clirarplugin.

The proprietary unrar program does not accept directories with a
trailing slash when they are passed to it when extracting some files
(think drag'n'drop).

BUG: 272281
FIXED-IN: 4.6.4

------------------------------------------------------------------------
r1234363 | lueck | 2011-06-01 08:54:18 +1200 (Wed, 01 Jun 2011) | 3 lines

make EMailTemplate string translatable
CCMAIL:kde-i18n-doc@kde.org
BUG:272637
------------------------------------------------------------------------
r1234597 | rkcosta | 2011-06-02 08:35:06 +1200 (Thu, 02 Jun 2011) | 122 lines

Backport r1216556, r1216557, r1216558, r1216559, r1216762, r1216763, r1224452, r1233140 and r1233141 (!!).

This should make password support in header-protected archives work again.

As support for password-protected archives with unencrypted headers
was already present, this should finish minimally resolving bug 35371
once more. More work on user-visible messages (status messags, error
messages etc) is probably needed, but not essential.

BUG: 35371
FIXED-IN: 4.6.4

--

r1216556 by rkcosta:

cliinterface: Move the qRegisterMetaType code to the constructor.

It is going to be run only once anyway, so it does not make much sense
to run the check in every process call.

--

r1216557 by rkcosta:

cliinterface: Merge create the process creation and execution methods.

Inpired by what Okular does in its comicbook generator, it also makes
sense for us to remove the process we run in the same method (ie
executeProcess()).

findProgramAndCreateProcess, createProcess and executeProcess have
been merged: the process is now crated, run and destroyed in the same
method, which makes us not need to wory about deleting the process in
different places.

While at it, doKill()'s implementation could also be simplified, as we
waitForFinished() right after calling KProcess::start(), so we need
not call it a few more times in doKill().

--

r1216558 by rkcosta:

cliinterface: Use KPtyProcess on non-Windows platforms.

Once again inspired by Okular's comicbook generator (and also by Ark's
code circa the 4.2 release, which also seems to have borrowed the same
ideas from Okular), we now use a KPtyProcess on non-Windows platforms.

Some programs (such as unrar) open a TTY in occasions such as showing
password prompts, as they usually use getpass(3).

This is the first step towards supporting header-protected rar and 7z
files.

--

r1216559 by rkcosta:

cliinterface: Add the PasswordPromptPattern parameter.

Plugins can now specify a regular expression that matches password
prompts, which are now detected by CliInterface.

Header-protected rar and 7z files should be working now.

--

r1216762 by rkcosta:

archivebase: Run user queries on the internal ListJob.

After the recent commits, a password may be prompted on the archive's first
listing (listIfNotListed()) if the archive is header-protected.

We now process this occasional user query, so Ark does not get stuck when
extracting header-protected files in batch extraction mode.

--

r1216763 by rkcosta:

cliinterface: Remove m_program, as it is not needed anymore.

--

r1224452 by rkcosta:

cliinterface: Set the correct boolean value after QEventLoop::exec().

QEventLoop::exec() does not return an int, but a bool, and 0 means it
ran successfully -- however, it will translate into false after the
implicit cast, which is the inverse of what we want.

--

r1233140 by rkcosta:

cliinterface: Wait a few seconds before killing the process.

In some cases, failOperation() will try to kill an application which
is already in the process of finishing gracefully, so it is better to
wait a few seconds before trying to kill the process.

--

r1233141 by rkcosta:

cliinterface: Do not reset the password after a wrong one was entered.

This partially reverts r964726. There does not seem to be any solid
reason for resetting the password like this, and there is at least one
reason not to do so: when in batch-extraction mode, addExtraction()
will create a ListJob to check if an archive has a single folder. If
the archive is header-encrypted, it will ask for a password. In case a
wrong password is entered, preserving it means the coming
ExtractionJob will use the wrong password and finally show the
"Incorredt password" error message which was not shown before (and
cancel the forthcoming jobs).

This should fix the issue in comment #75 of bug 35371.
------------------------------------------------------------------------
r1234838 | dakon | 2011-06-03 03:58:08 +1200 (Fri, 03 Jun 2011) | 6 lines

fix some actions showing up for toolbars not having a text

BUG:272619

backport of r1234835 from trunk

------------------------------------------------------------------------
