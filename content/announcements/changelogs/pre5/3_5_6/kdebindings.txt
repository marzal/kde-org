2006-10-11 14:34 +0000 [r594517]  mueller

	* branches/KDE/3.5/kdebindings/python/sip/siplib/sip.h,
	  branches/KDE/3.5/kdebindings/python/sip/siplib/siplib.c: adapt to
	  changed python 2.5 API. Unfortunately thats not enough, it still
	  crashes in weird ways. Thank god we have this outdated forked
	  copy here and not upstream, which works fine.

2006-10-11 22:54 +0000 [r594670]  mueller

	* branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kcompletion.sip,
	  branches/KDE/3.5/kdebindings/python/sip/sipgen/gencode.c,
	  branches/KDE/3.5/kdebindings/python/pyqt/sip/qt/qdir.sip,
	  branches/KDE/3.5/kdebindings/python/Makefile.am,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/bytearray.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kmacroexpander.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kmdi/kmdichildfrm.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kio/global.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kparts/browserextension.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdeui/kkeydialog.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kconfigdata.sip,
	  branches/KDE/3.5/kdebindings/python/pyqt/sip/qt/qstringlist.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kurl.sip,
	  branches/KDE/3.5/kdebindings/python/pyqt/sip/qt/qstring.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kconfig.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kdecore/kaccel.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kio/authinfo.sip,
	  branches/KDE/3.5/kdebindings/python/pykde/sip/kio/kservicetype.sip:
	  fix the bindings for python 2.5/64 bit

2006-10-22 19:08 +0000 [r598176]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/Korundum.cpp,
	  branches/KDE/3.5/kdebindings/korundum/ChangeLog: * Special case
	  QValueList<int> as a DCOPRef return type. Fixes problem reported
	  by Brian Bart. CCMAIL: kde-bindings@kde.org

2006-10-24 18:00 +0000 [r598814]  rdale

	* branches/KDE/3.5/kdebindings/qtjava/javalib/org/kde/qt/Invocation.java,
	  branches/KDE/3.5/kdebindings/qtjava/ChangeLog: * Fixed compiler
	  warning in Invocation.java reported by Rafael Lopez CCMAIL:
	  kde-java@kde.org

2006-11-09 18:59 +0000 [r603687]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/examples/RubberDoc.rb:
	  * Fix error in KDE::CmdLineArgs.init() call

2006-11-20 17:22 +0000 [r606521]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Made
	  Qt::ListView, Qt::ListViewItem, Qt::BoxLayout, Qt::HBoxLayout,
	  Qt::VBoxLayout and Qt::GridLayout Enumerable with implementations
	  of each() so they don't need to use the Qt External iterators
	  like Qt::LayoutIterator anymore. For instance: lv =
	  Qt::ListView.new do ["one", "two", "three", "four"].each do
	  |label| Qt::ListViewItem.new(self, label, "zzz") end end lv.each
	  do |item| p item.inspect pp item.inspect end * Add inspect() and
	  pretty_print() methods to Qt::ListViewItem so that they show the
	  text of their columns CCMAIL: kde-bindings@kde.org

2006-11-21 10:38 +0000 [r606663]  rdale

	* branches/KDE/3.5/kdebindings/korundum/ChangeLog,
	  branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * Made KDE::ListView, KDE::ListViewItem Enumerable with
	  implementations of each() so they don't need to use the Qt
	  External iterators like Qt::ListViewItemIterator anymore. For
	  instance: lv = KDE::ListView.new do ["one", "two", "three",
	  "four"].each do |label| KDE::ListViewItem.new(self, label, "zzz")
	  end end lv.each do |item| p item.inspect pp item.inspect end *
	  Add inspect() and pretty_print() methods to KDE::ListViewItem so
	  that they show the text of their columns

2006-12-05 02:13 +0000 [r610700]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/smokeruby.h: *
	  Backported some improvements to Qt::Variants from Qt4 QtRuby *
	  When marhalling QMap types with QVariant values, if the Ruby
	  value isn't a Qt::Variant then one is created * Qt::Variants can
	  now be constructed with Hash's of String/Qt::Variant pairs, and
	  from Arrays of Qt::Variants * When marshalling
	  QValueList<QVariant> types from Ruby to C++, if any elements
	  aren't Ruby Qt::Variants, they are automatically converted.
	  Hence, the following will work: v = Qt::Variant.new([1, 2, 3])
	  The Qt::Variant v will contain a list of 3 QVariants with
	  typeName 'int' * Change all instances of strcmp() to qstrcmp()
	  CCMAIL: kde-bindings@kde.org

2006-12-05 02:23 +0000 [r610701]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb:
	  * Fix wrong value of newly added Qt::Variant constant

2006-12-05 16:25 +0000 [r610800]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/smokeruby.h: *
	  Add an include for qcstring.h, fixes problem reported by annma

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

