---
aliases:
- /announcements/plasma-5.7.0-5.7.1-changelog
hidden: true
plasma: true
title: Plasma 5.7.1 Complete Changelog
type: fulllog
version: 5.7.1
---

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Fix icon type assignment in toolbars to re-enable disabled icons. Thanks Craig for finding and fixing. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5f47df2705a81aaa4f1d63039a13c27faf38fe52'>Commit.</a>

### <a name='breeze-plymouth' href='http://quickgit.kde.org/?p=breeze-plymouth.git'>Breeze Plymouth</a>

- Some style cleanup. <a href='http://quickgit.kde.org/?p=breeze-plymouth.git&amp;a=commit&amp;h=49691bf5a29cc112ee87c77c29f3cd7ad7e9f8e3'>Commit.</a>
- Prevent background paint problems on some nvidia systems. <a href='http://quickgit.kde.org/?p=breeze-plymouth.git&amp;a=commit&amp;h=4d1d0fe899e493ba4754e4f6191f09533d55dd53'>Commit.</a>
- Expand test to replicate what packagekit does for updating. <a href='http://quickgit.kde.org/?p=breeze-plymouth.git&amp;a=commit&amp;h=de0cafe4c71274fe8d5d388608e98dadcee72bd5'>Commit.</a>
- Fix spinner height and y to resolve broken layout chain. <a href='http://quickgit.kde.org/?p=breeze-plymouth.git&amp;a=commit&amp;h=a5909e1cd59f59254898421df2eb66d4fe10e393'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365169'>#365169</a>
- Fix style. <a href='http://quickgit.kde.org/?p=breeze-plymouth.git&amp;a=commit&amp;h=dc6c4b39eb88f621046b6093354e0a2920702203'>Commit.</a>

### <a name='kgamma5' href='http://quickgit.kde.org/?p=kgamma5.git'>Gamma Monitor Calibration Tool</a>

- Cmake: look for and use explictly used frameworks. <a href='http://quickgit.kde.org/?p=kgamma5.git&amp;a=commit&amp;h=29ffb29dac036ef6450b279435670e39ef40fe96'>Commit.</a>
- Remove unneeded KDialog usage. <a href='http://quickgit.kde.org/?p=kgamma5.git&amp;a=commit&amp;h=877decf18684fd4a163fcae80cc888015e45d53c'>Commit.</a>

### <a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a>

- Add a shortcut for Meta+P. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=8201d07e4b800e82c1f620d506f56f7d4388f938'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365147'>#365147</a>
- Fix build with clang 3.8. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=329bd01ec291ba9e73a5eae6870a5c9770951d8b'>Commit.</a>
- Handle DVI. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=ab629a30c398c870d0ca190582732f6023994db4'>Commit.</a>
- Do not 'default' handle switch-cases. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=b484e6d6bf27a8b09809abafb9b12475d4f5c101'>Commit.</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Fix shadow rendering calculations. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=62d09fad123d9aab5afcff0f27d109ef7c6c18ba'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365097'>#365097</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Remove debug. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9ade4105e0741bec29fca0ad4237ecb8f10a48f3'>Commit.</a>
- Fix window preview activation/close button for single-window tooltips. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=901e76e4b0fbf91a9f4c490f392514deb1989dc5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365227'>#365227</a>
- [Task Manager] Reject wheel event if switching windows by mouse wheel is disabled. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e30a37a514026fc52c0b8ff7aa479d5b955ebf3e'>Commit.</a>

### <a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a>

- Fix microphone increase/decrease volume actions. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=9f9d980414ef6b12d24bde8cf2967b519be29524'>Commit.</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Consider the primary screen as default screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d1210f5fda017be0696ebb267901811cabb82215'>Commit.</a>
- [System Tray] Adjust shortcut column width if shortcut changes. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=240d2e89be0c940b96aaea02178a5a2d6c16670d'>Commit.</a>
- Don't recurse. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f631964f2d119493968a23a38dd053f2c57a8197'>Commit.</a>
- Return LegacyWinIdList for groups in final proxy sort order. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=313aede08506b87358e0d770911c562d3ac6f234'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365181'>#365181</a>
- Avoid side-channeling through shared static source models. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e2b07027de7cb15171a35aa59e8b90397b5c47b7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365011'>#365011</a>
- Make the systray work with scripting shell again. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6adba6315275f4ef6ebf8879c851830c268f7a51'>Commit.</a>
- [System Tray] Use TableView viewport to determin column width. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2fec0af93ccf96278697ffbb002c1e8d205257e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365099'>#365099</a>
- [System Tray] Increase maximum icon size. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1ba0e6e5ddd598e9b0cf87a654467f97c06b26c5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364431'>#364431</a>
- [Digital Clock] Hide agenda if no calendar event plugins are enabled. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cb758b15a623fc4edaaaf7383c307d9c41892a0f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364455'>#364455</a>
- Fix files/folders in desktop not opening with right click context menu. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=6911541421dc068cee531a5fd5f322c0db5d7492'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364530'>#364530</a>