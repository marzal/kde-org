---
aliases:
- /announcements/plasma-5.18.1-5.18.2-changelog
hidden: true
plasma: true
title: Plasma 5.18.2 Complete Changelog
type: fulllog
version: 5.18.2
---

### <a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a>

- Set all labels to plain text. <a href='https://commits.kde.org/bluedevil/fb1a8474651ef1ab5861b9221e41ff56e3d814e6'>Commit.</a> See bug <a href='https://bugs.kde.org/417980'>#417980</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27543'>D27543</a>
- Add explicit includes to fix build with Qt 5.11. <a href='https://commits.kde.org/bluedevil/178e5fadfb4fc01695bb14436c21734bdb83426c'>Commit.</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Properly react to failed transactions. <a href='https://commits.kde.org/discover/5a76f88f503378794afa9a03491984782d464a30'>Commit.</a>
- Packagekit: Fix updatesCountChanged signal emission. <a href='https://commits.kde.org/discover/b3ad91860e7f91e401fbe8e6444cd7c243a1d416'>Commit.</a>
- Flatpak: fix build on old flatpak versions. <a href='https://commits.kde.org/discover/7ff2de8d54ae749a142856c440816e764bfe5628'>Commit.</a>
- Flatpak: after performing operations, check for unused refs. <a href='https://commits.kde.org/discover/9c287d7cbaf4e9e4f97fbd86616b910d1b37e9a8'>Commit.</a>
- Flatpak: make sure the progress isn't over 100. <a href='https://commits.kde.org/discover/d128b5b1b0b888fdda5a8131569be215eb56662c'>Commit.</a>
- Flatpak: provide the arch when checking the timestamp. <a href='https://commits.kde.org/discover/47f370d03df24b132faa0c302b181d21e7beec64'>Commit.</a>
- Flatpak: reduce leakage of GObjects. <a href='https://commits.kde.org/discover/74ac48adebd8ab4fe8de1c2a41e1504cccdd1c09'>Commit.</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [applets/weather] Remove outdated extra configure button. <a href='https://commits.kde.org/kdeplasma-addons/2c7737f8fe8d9c60426df81a426557f3cd681508'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412114'>#412114</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27529'>D27529</a>
- Fix condition in KonsoleProfiles::run. <a href='https://commits.kde.org/kdeplasma-addons/f0431c95a8957a9d211b1eeb52445aaba67b339c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27365'>D27365</a>

### <a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a>

- Unify ksysguard cpu clock speed names. <a href='https://commits.kde.org/ksysguard/4e656a45df16565e4273ae67d8dc4d530b5ca488'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D26857'>D26857</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Schedule a workspace repaint in AbstractClient::minimize(). <a href='https://commits.kde.org/kwin/275082ad49919b848ec3db715345e48741c33eb0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27516'>D27516</a>
- [plastik] Disable problematic text hints. <a href='https://commits.kde.org/kwin/39798bffb1398d6b75a067ff5552a63ea6b182f7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417699'>#417699</a>. Fixes bug <a href='https://bugs.kde.org/413179'>#413179</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27488'>D27488</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Fix: handle when backend fails to load/initialize. <a href='https://commits.kde.org/libkscreen/ff98585ea5541012b68604e34b7fec383a487cd9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27625'>D27625</a>
- Fix(kwayland): wait longer for connection timeout and retry. <a href='https://commits.kde.org/libkscreen/691c85879cafad942edf6245196da9f04bc4cb6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416566'>#416566</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27618'>D27618</a>
- Fix(kwayland): remove code with side-effects from Q_ASSERT. <a href='https://commits.kde.org/libkscreen/63cab28e74ee30706cc43958921ae4224e4ea1fc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413892'>#413892</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27536'>D27536</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Calculate needsSave on checkbox toggle not on change. <a href='https://commits.kde.org/plasma-desktop/1dd4668f1bf15e057b841ce6e538073f1538ec27'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411584'>#411584</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27590'>D27590</a>
- Remove attempt to start long gone baloo_file_cleaner. <a href='https://commits.kde.org/plasma-desktop/c4871cbc514f46c6fb04217c993065f39e0a729c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27579'>D27579</a>
- [kcm/baloo] Clip ListView. <a href='https://commits.kde.org/plasma-desktop/0ec354f7d26926b9c2c945f3509db8ac9eea4122'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27559'>D27559</a>
- [kcm/activities] Clip ListView. <a href='https://commits.kde.org/plasma-desktop/dba3d567027540f66ecacc677b1d78f25b3759a0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27561'>D27561</a>
- Emojier: use proper scoping for QPixmap. <a href='https://commits.kde.org/plasma-desktop/939e9eb39d62e3c1895a8dc33fe6e117cb36b4e3'>Commit.</a>
- KCM look and feel : set proper default values for kwin kdecoration when applying global style. <a href='https://commits.kde.org/plasma-desktop/56fd983308d1e86d63db9d566dabd0967df91133'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27483'>D27483</a>
- Emojier: --warnings. <a href='https://commits.kde.org/plasma-desktop/a69f20a0bb68a41caed8cc98c76f689d236411c8'>Commit.</a>
- Emojier: improve the fallback mechanism to detect languages. <a href='https://commits.kde.org/plasma-desktop/358e98a75a946abe76ffdfeddd0156483a66d4b3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417713'>#417713</a>
- Emojier: properly specify the application component name to KAboutData. <a href='https://commits.kde.org/plasma-desktop/c92e4845397d7abe3905ec7d31a53ab17d4316f0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/416669'>#416669</a>
- Emojier: Only fit text when we know it's a very complex emoji. <a href='https://commits.kde.org/plasma-desktop/91c446e0c4430289369963bf44e64228add67d18'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417454'>#417454</a>
- Emojier: make it possible to filter by annotation. <a href='https://commits.kde.org/plasma-desktop/7cd13d1470e9a608a1bd2605caa42646394b4882'>Commit.</a>
- [Folder view]Make shadow scale aware. <a href='https://commits.kde.org/plasma-desktop/4056e24b353dad3c7bdfb2221a5ba57709ea8932'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27316'>D27316</a>
- Set good default value for cursor theme and size in KRDB. <a href='https://commits.kde.org/plasma-desktop/ca9f0eeb9b5377a598c6ec58aa1a2beaa08d69c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414109'>#414109</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27470'>D27470</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Fix build with missing includes. <a href='https://commits.kde.org/plasma-nm/9a603a90fd5d031c778b38d1f4b3c06ce6ae6389'>Commit.</a>
- [kded] Set password dialog boxes to plaintext. <a href='https://commits.kde.org/plasma-nm/762504196246af2947a3a113f1a57fac7942aab0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27541'>D27541</a>
- [kded] Escape notifications. <a href='https://commits.kde.org/plasma-nm/abe0ed15ec5a6d7090450886f85492904c62faea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27542'>D27542</a>
- [kcm] Escape connection name in close dialog. <a href='https://commits.kde.org/plasma-nm/28c36476fc3b6fe934479dfe8f24eae59c5ccad9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27545'>D27545</a>
- KCM: Fix connection list background and padding. <a href='https://commits.kde.org/plasma-nm/04cd1f337fef85f319f26edfb3edf2fc7bf7c551'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27382'>D27382</a>
- Fix ampersand and other characters in tooltip. <a href='https://commits.kde.org/plasma-nm/9e4db88fe6010b593cbcde433044aa5b20b1de68'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398225'>#398225</a>
- Make sure we change icon on NM connectivity change. <a href='https://commits.kde.org/plasma-nm/0491738ced86a970c4ac75057da2b8b4eb350cdd'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Use appropriate icon for unconfigured applets' "Configure..." buttons. <a href='https://commits.kde.org/plasma-workspace/cb46e52aad66a2b6eeb75ac0de630302729b90d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27530'>D27530</a>
- [applets/systemtray] Clip ListView in config. <a href='https://commits.kde.org/plasma-workspace/193c89a4a0cfdd9d1940b28172400acee5a57ed3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27570'>D27570</a>
- [kcms/feedback] Increase default window height a bit more. <a href='https://commits.kde.org/plasma-workspace/be4a0ae13a52a361cb413a35fe9b08d326240f9f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417747'>#417747</a>
- Fix activity runner. <a href='https://commits.kde.org/plasma-workspace/2a979a4bbebecaade6d4665337b6c297cc22a866'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27367'>D27367</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- Fix compilation with Qt 5.15, this hit the time bomb too. <a href='https://commits.kde.org/powerdevil/0fa63b8685f82e5f626058dfc0f9461ae158599b'>Commit.</a>