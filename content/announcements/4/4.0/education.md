---
custom_about: true
custom_contact: true
hidden: true
title: 'KDE 4.0 Visual Guide: Educational Applications'
---

<p>
The KDE Edu team creates high-quality educational software. Target age varies 
from 3 to 99 years, aiding schoolchildren, university students and teachers 
alike. The main focus generally is schoolchildren.
</p>

<h2>Kalzium	</h2>
<p>
Kalzium shows information about chemical elements. Click on 'State of Matter' on 
the left and adjust the slider to see the state of elements at a certain 
temperature. The timeline shows in a similar way the year an element 
was discovered in. In the Calculate tab you can enter a chemical formula, for 
example: C2H5Br. Kalzium will show you what atoms the molecule consists of and 
its molecule mass.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kalzium-stateofmatter.png">
<img src="/announcements/4/4.0/kalzium-stateofmatter_thumb.png" class="img-fluid">
</a> <br/>
<em>Kalzium showing the state of matter</em>
</div>
<br/>

<p>
Click on the Equation Solver to enter a chemical equation and let Kalzium 
compute the results. A sample formula is entered by default, so just click on Calculate to see Results.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/kalzium-molview.png">
<img src="/announcements/4/4.0/kalzium-molview_thumb.png" class="img-fluid"><br/></a>
<em>Check out molecules in 3d in Kalzium </em>
</div>
<br/>

<p>
Click on the Molecular Viewer to be able to view 3D models of Molecules. Several 
examples are supplied with Kalzium. Click on "Load Molecule" and select one of 
the files presented in the file open dialog. You can rotate the Molecule by 
holding the right mousebutton and moving the mouse. The mousewheel will zoom in on 
the molecule, and the left mouse button can move the molecule around. The 
options under Display allow you to adjust quality and the way the molecule is 
displayed. Click the dropdown menu labeled 'Balls and sticks' 
and choose Van der Waals to see the Van der Waals force. Kalzium support many 
file formats of molecular data through <a 
href="http://openbabel.sourceforge.net/wiki/Main_Page">OpenBabel</a>.
</p>

<h2> Parley	Vocabulary Trainer</h2>

<div class="text-center">
<a href="/announcements/4/4.0/parley.png">
<img src="/announcements/4/4.0/parley_thumb.png" class="img-fluid">
</a> <br/>
<em>Learn foreign languages with Parley</em>
</div>
<br/>

<p>
Parley is an application to learn foreign language vocabularies or any other list of words or objects. Click on the File menu and choose "Download New Vocabularies" to 
chose one to learn. Choose "Basic Vocabulary German-English" to learn 
German. Click on "Start Practice" to start practicing the language. Answer the 
question and click on Verify to see if you were right. When you click Stop 
Practice, Parley will show you how well you did. Under the Practice menu you can 
find Statistics about your progression.
</p>

<h2> Marble	Desktop Globe</h2>

<div class="text-center">
<a href="/announcements/4/4.0/marble.png">
<img src="/announcements/4/4.0/marble_thumb.png" class="img-fluid">
</a> <br/>
<em>The Marble Desktop Globe</em>
</div>
<br/>

<p>
Marble is an application which shows you a three-dimensional map globe which you 
can rotate and view. Use your mousewheel to zoom in and out. Grab the globe 
with your mouse to drag it around. Click on a city and choose the Wikipedia tab to 
see more information about that town, including a pictures and links with more 
information. If you click on Map View on the left, you can choose other ways 
to view earth, including flat projection like a real map, plain maps and the earth 
at night. If you hold your mouse over the theme picture, you will see information 
about the map. Click on the File menu and choose Download New Data to download 
more earth themes. Through "Get Hot New Stuff", KDE's integration system for
online resources, you can -- from within Marble -- download new maps to view in
Marble.
</p>

<h2> Blinken Memory Trainer</h2>

<div class="text-center">
<a href="/announcements/4/4.0/blinken.png">
<img src="/announcements/4/4.0/blinken_thumb.png" class="img-fluid">
</a> <br/>
<em>Train memory and reaction with Blinken</em>
</div>
<br/>

<p>
Blinken is a little application to train your memory. Press the start button to begin the training. Now choose a difficulty level and wait until Blinken starts. It will light up one of the four corners, and then you must click it. After a while it will light up 2 corners consecutively, and again you have to repeat it. Try to get as far as you an!
</p>

<h2> KStars Desktop Planetarium	</h2>

<div class="text-center">
<a href="/announcements/4/4.0/kstars.png">
<img src="/announcements/4/4.0/kstars_thumb.png" class="img-fluid">
</a> <br/>
<em>Navigate the sky with KStars</em>
</div>
<br/>

<p>
KStars is an application which you can use to view the nightsky above. It can be 
used to control digital telescopes, even remotely over the internet. When you start it, 
you will be presented with a dialog asking you to select the your location. Once you 
have done so, it will move the view of the night sky to how it should look from 
where you are. You can grab the sky with the left mousebutton and move around, and 
zoom in with the mousewheel. Right click on an object to see more information 
about it and view images. Click on the Tools menu and choose What's up Tonight to 
see the list of heavenly objects which will be visible from your position. Select an object, and click Center Object to view that location. Click on 
Object Details to view detailed information about the Planet, Comet or other 
object you are viewing. Under the Position tab you can see the exact Coordinates 
of this object. Under the Links tab you can see images and informational 
links about the object. You can record your observations under Log. If you 
have a digital telescope, you can start the Telescope Wizard under the Devices 
menu, and control your telescope from within KStars. Use the Time menu to select 
a certain time and date to view the sky. You can make time go faster, slower 
or pause using the time buttons in the toolbar.
</p>

<h2>KTouch Touch Typing Trainer</h2>

<div class="text-center">
<a href="/announcements/4/4.0/ktouch.png">
<img src="/announcements/4/4.0/ktouch_thumb.png" class="img-fluid">
</a> <br/>
<em>Train your touch typing skills using KTouch</em>
</div>
<br/>

<p>
KTouch is an application that helps you to learn touch typing on your 
computer. After starting it, choose a lecture from the Default Lectures submenu 
in Training. You have to type the characters which appear in the top white 
bar correctly below. The keyboard picture shows you where the character you have 
to push next can be found. If you made a mistake, the bar you are typing in will 
turn red until you have corrected your mistake. KTouch will show your speed and
correctness in its main window, and you can see more information by clicking 
Lecture Statistics.
</p>
<p>
All the above are just examples of some applications in the KDE Education Project, many
more can be found on their <a href="http://edu.kde.org/">new website</a>.
</p>

<table width="100%">
	<tr>
		<td width="50%">
				<a href="../applications">
				<img src="/announcements/4/4.0/images/applications-32.png" />
				Previous page: Basic Applications
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="../games">Next page: Games
				<img src="/announcements/4/4.0/images/games-32.png" /></a>
		</td>
	</tr>
</table>