---
title: "KDE rakendused: hõlpsamad kasutada, parema jõudlusega ja viivad lausa Marsile"
date: "2013-02-06"
hidden: true
---

## Kate märguanded pole enam nii pealetükkivad

KDE võimas tekstiredaktor Kate sai täiustusi paljudes valdades. Tänu <a href="http://ev.kde.org">KDE e.V.</a> spondeerimisel peetud intensiivsetele <a href="http://dot.kde.org/2012/11/24/katekdevelop-october-sprint-whats-new-kate">koodikirjutamispäevadele oktoobris</a> sai Kate tunduvalt parema märguannete süsteemi, lisavõimalusena kerimisribana kasutatava niinimetatud minikaardi, uue projektihalduse plugina, valmisvärviskeemid, tunduvaid parandusi skriptiliideses ja veel palju muud. Tõsise vigade parandamise pingutusega suudeti avatud veateadete hulk viia 850lt 60ni. Koodikirjutamispäevade kõrval nähti vaeva uue kiiravamise võimaluse ja muude vajalike omaduste kallal. Kõik parandused toovad kasu ka Kate kasutamisele teistes rakendustes, sealhulgas kergekaalulisemale tekstiredaktorile KWrite ning KDE võimsale arenduskeskkonnale KDevelop.

<div class="text-center">
	<a href="/announcements/4/4.10.0/kate-notifications.png">
	<img src="/announcements/4/4.10.0/thumbs/kate-notifications.png" class="img-fluid">
	</a><br/>
  <em>
  Kate uued passiivsed märguanded ei sega enam nii palju töötegemist
  </em>
</div>
<br/>

## Konsooli täiustused

Konsooli jõudsid tagasi KDE 3 ajast tuntud ekraani trükkimise ja signaalide saatmise võimalused, samuti saab muuta ridade vahet ning Control-klahvi käitumist teksti lohistamise ajal. Nüüd on toetatud xtermi 1006 hiirelaiendused ning käsurea võib puhastada, enne kui kasutada järjehoidjaid mõne käsu andmiseks.

<div class="text-center">
	<a href="/announcements/4/4.10.0/konsole-top.png">
	<img src="/announcements/4/4.10.0/thumbs/konsole-top.png" class="img-fluid">
	</a><br/>
  <em>
  Konsool sai uusi võimalusi
  </em>
</div>
<br/>

## Tükkrenderdamine parandab Okulari jõudlust

KDE universaalne dokumendinäitaja Okular sai hulga täiustusi. Uusimate omaduste hulka kuulub näiteks meetod, mida nimetatakse tükkrenderdamiseks (<a href="http://tsdgeos.blogspot.com/2012/11/okular-tiled-rendering-merged-to-master.html">tiled rendering</a>), mis annab Okularile võimaluse suurendada dokumente senisest rohkem ja kiiremini ning samal ajal ometi vähendada varasemaga võrreldes mälukasutust. Põimitud video käitlemist on täiustatud. Annotatsioonide loomine ja muutmine on kasutajasõbralikum ning nüüd käitub ka tahvelarvuti annotatsiooni loomisel samamoodi nagu hiirega nii-öelda päriarvuti. Taustal tegutseva meetodi (QTabletEvent) suure täpsuse tõttu on ka vabakäeannotatsioonid palju kenamad. Üks uus omadus on hõlpus liikumine ajaloos edasi- ja tagasinupuga. Dokumendinäitaja puutesõbralik versioon Okular Active kuulub nüüd KDE tuumikrakenduste hulka. See on Plasma Active'i uus e-raamatute lugemise rakendus, mida on optimeeritud puutetundlikke seadmeid silmas pidades. Okular Active toetab väga paljusid e-raamatu vorminguid.

<div class="text-center">
	<a href="/announcements/4/4.10.0/okular-tiled-rendering.png">
	<img src="/announcements/4/4.10.0/thumbs/okular-tiled-rendering.png" class="img-fluid">
	</a><br/>
  <em>
  Tükkrenderdamine Okularis: kiirem suurendamine vähema mäluga
  </em>
</div>
<br/>

## Gwenview sai tegevuste toe

KDE pildinäitaja Gwenview võib uhkustada täiustatud pisipiltide loomise ja käsitsemise ning tegevuste toetamisega. Rakendus toetab JPG- ja PNG-failide värvikorrektsiooni, kohandudes koostöös KWiniga konkreetse monitori värviprofiilile, mis lubab fotosid ja graafikat eri masinates ühesugusena näha. Gwenview piltide importija töötab nüüd rekursiivselt, näidates kõiki saadaolevaid pilte nii kataloogi sees kui ka selle alamkataloogides. Täpsemalt kõneleb sellest <a href="http://agateau.com/2012/12/04/changes-in-gwenview-for-kde-sc-4.10">Gwenview hooldaja Aurélien Gateau ajaveeb</a>.

<div class="text-center">
	<a href="/announcements/4/4.10.0/gwenview.png">
	<img src="/announcements/4/4.10.0/thumbs/gwenview.png" class="img-fluid">
	</a><br/>
  <em>
  KDE tõhus pildinäitaja Gwenview
  </em>
</div>
<br/>

KDE vestlusrakendus Kopete sai tõelise "mittehäirimise" režiimi, mis tühistab täielikult visuaalsed ja helilised märguanded.

## Kontacti jõudlus paranes

KDE PIM-i rakendustes parandati palju vigu ja lisati hulk täiendusi. Suur töö otsinguga tegeleva taustaprogrammi kallal tõi kaasa tohutult paranenud e-kirjade indekseerimise ja hankimise, mis omakorda tähendab töökindlamaid rakendusi ja väiksemat ressursikasutust.

<div class="text-center">
	<a href="/announcements/4/4.10.0/kontact.png">
	<img src="/announcements/4/4.10.0/thumbs/kontact.png" class="img-fluid">
	</a><br/>
  <em>
  Kontacti grupitöö klient
  </em>
</div>
<br/>

KMail suudab nüüd automaatselt muuta kirjadele lisatud piltide suurust, mida saab seadistada rakenduse seadistustes. Samuti pakub KMail teksti automaatset korrigeerimist, muu hulgas sõnade asendamist ja lausete algustähtede muutmist suureks. Seadistused ja sõnaloendid on ühised Calligra Wordsiga ning neid saab igati muuta. Laienenud on HTML-kirjade koostamise toetus: lisada saab tabeleid, määrata nende ridade ja veergude omadusi ning lahtreid liita. Nüüd on toetatud ka piltide kindlaks määratud suurused, samuti võib HTML-koodi otse sisestada. Paranenud on samuti HTML-kirjade "lihtteksti" osa, sealhulgas HTML-siltide teisendamine lihttekstiks. Muude KMaili täiustuste seas võib ära mainida hiljuti kasutatud failide avamist kirjakoostajas, uute kontaktide lisamist otse KMailist ning visiitkaartide (vCard) lisamist kirjadele.<br />
Importimisnõustaja toetab nüüd seadistuste importimist Operast, seadistuste ja andmete importimist Claws Mailist ja Balsast ning siltide importimist Thunderbirdist ja Claws Mailist.

<div class="text-center">
	<a href="/announcements/4/4.10.0/kontact-mail.png">
	<img src="/announcements/4/4.10.0/thumbs/kontact-mail.png" class="img-fluid">
	</a><br/>
  <em>
  Kontacti e-posti klient
  </em>
</div>
<br/>

## Suured täiustused mängudes

KDE mängud ja õpirakendused on üle elanud suuri muutusi. KDE mängud on tunduvalt paremad põhiteekide täiustamise tõttu, mis tagab märksa sujuvama mängimise. Välimuselt ja sisultki uus on masinakirja õppimise rakendus KTouch ning etemaks on mitmes mõttes muutunud Marble, mis vaid kindlustab enda tugevat kohta vaba tarkvara maailma kaardirakenduste seas.

Käesolevas väljalaskes on samuti üks uus mäng. <a href="http://games.kde.org/game.php?game=picmi">Picmi</a> kujutab endast ühele inimesele mõeldud loogikamängu. Selle eesmärk on märkida lahtreid vastavalt mängulaua serval olevatele arvudele, et luua algselt varjatud muster või pilt. Picmi pakub kaht mängimisrežiimi: juhuslikud mängud luuakse vastavalt valitud raskusastmele, kuid võib läbi mängida ka valmismõistatusi.

<div class="text-center">
	<a href="/announcements/4/4.10.0/picmi.png">
	<img src="/announcements/4/4.10.0/thumbs/picmi.png" class="img-fluid">
	</a><br/>
  <em>
  Uus mäng Picmi
  </em>
</div>
<br/>

## Sudokude trükkimine

Parandusi on saanud teisedki KDE mängud ja õpirakendused, muu hulgas suudab KSudoku nüüd mõistatusi välja trükkida, nii et neid saab lahendada ka mujal kui arvutis. KGoldrunner on ümber kirjutatud uute KDE mängude teekide peale: mäng ise ja välimuski on sama, aga kõik toimib kenamini ja sujuvamalt. Hüpleva kuubiku mäng (KJumpingCube) võimaldab nüüd muuta käikude kiirust ning animeerib mitmeosalised käigud, et neid oleks lihtsam mõista. Kasutajaliidest on täiustatud ja mängida saab kas Kepleri või Newtoni vastu. Väiksema mängulaua korral saab kasutada lihtsustatud mängustiile. KAlgebra kasutajaliidest on veidi parandatud, Pairs pakub aga võimalust kasutada teemaredaktorit.

<div class="text-center">
	<a href="/announcements/4/4.10.0/ksudoku.png">
	<img src="/announcements/4/4.10.0/thumbs/ksudoku.png" class="img-fluid">
	</a><br/>
  <em>
  Sudokusid saab trükkida ja paberil lahendada
  </em>
</div>
<br/>

Graafiteooria arenduskeskkonna Rocs arendajad muutsid omajagu kasutajaliidest ja seadistustedialoogi, et neid oleks hõlpsam kasutada. Samuti on nüüd toetatud järgmised failitüübid: TGF, DOT/Graphviz (import/eksport) ja TikZ/PGF (ainult eksport).

## Kuule ja Marsile: kosmosesondid kõigile näha

<a href="http://marble.kde.org/">Virtuaalne gloobus Marble</a> jätkab tungimist kosmosesse. <a href="http://tu-dresden.de/en">Dresdeni tehnikaülikooli</a> tudeng René Küttner võttis Marble'i ette <a href="http://sophia.estec.esa.int/socis2012/">ESA SoCiS 2012 programmi</a> raames. <a href="http://www.esa.int/ESA">Euroopa Kosmoseagentuur</a> käivitas ka programmi "The Summer of Code in Space" ning sellegi puhul ostus Marble väljavalituks.

<div class="text-center">
	<a href="/announcements/4/4.10.0/marble-indonesia.png">
	<img src="/announcements/4/4.10.0/thumbs/marble-indonesia.png" class="img-fluid">
	</a><br/>
  <em>
  Töölauagloobus Marble
  </em>
</div>
<br/>

René töötas välja viisi, kuidas näidata teiste planeetide ümber tiirlevaid kosmosesonde Marble'is. Nii võib Marble näidata selliste kosmoseekspeditsioonide asukohti ja orbiite, nagu <a href="http://www.esa.int/Our_Activities/Space_Science/Mars_Express">Mars Express</a>, <a href="http://www.esa.int/Our_Activities/Space_Science/Venus_Express">Venus Express</a> ja <a href="http://www.esa.int/Our_Activities/Space_Science/SMART-1">SMART-1</a>. Näha on ka Marsi kuude Phobose ja Deimose asukohad. Samuti täiustas ta Maa satelliitide teekonna kuvamist. Mõningaid lisandunud omadusi näitab <a href="http://www.youtube.com/watch?v=K_VA0XtvjYk">see video</a>. Projekti rahastamise eest tuleb tänada <a href="http://www.esa.int/ESA">ESA-t</a> ja <a href="http://sophia.estec.esa.int/socis2012/?q=sponsors">SoCiS-e sponsoreid</a>.

<div class="text-center">
	<a href="/announcements/4/4.10.0/marble-mars.png">
	<img src="/announcements/4/4.10.0/thumbs/marble-mars.png" class="img-fluid">
	</a><br/>
  <em>
  Marble Marsil
  </em>
</div>
<br/>

Sellest projektist ja muudest Marble'i uudistest annab põhjalikuma ülevaate <a href="http://marble.kde.org/changelog.php#v15">Marble'i visuaalne muudatuste logi</a>.

## Masinakirja õppimine mõnusaks

KDE masinakirja õppimise rakendus Ktouch on põhjalikult ümber kirjutatud. Nüüd on sel selge, elegantne ja elav kasutajaliides, mis muudab masinakirja õppimise ja harjutamise nii mõnusaks, kui see vähegi olla saab. <a href="http://blog.sebasgo.net/blog/2012/11/28/november-update-for-ktouch/">Uus kasutajaliides</a> vähendab varasemat keerukust ning annab õppurile abi värvide ja mittesegavate animatsioonidega. Paljud uued omadused aitavad õppimist ja harjutamist veelgi parandada: uus õppetunniredaktor kasutab kvaliteedikontrolli, kasutaja saab jälgida enda edenemist ja leida üles nõrgad kohad, üldine välimus on köitev ja vastab ekraani suurusele, vihjeid ja konkreetseid osutusi, mida ja kuidas paremaks muuta, näeb palju selgemini.

<div class="text-center">
	<a href="/announcements/4/4.10.0/ktouch.png">
	<img src="/announcements/4/4.10.0/thumbs/ktouch.png" class="img-fluid">
	</a><br/>
  <em>
  Masinakiri selgeks KTouchiga
  </em>
</div>
<br/>

#### KDE rakenduste paigaldamine

KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral ja protsessoritel (näiteks ARM ja x86), operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta. <a href="http://plasma-active.org">Plasma Active</a> kujutab endast kasutajakogemust paljudele seadmetele, näiteks tahvelarvutid ja muud mobiilsed seadmed.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a href="http://download.kde.org/stable/4.10.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.

<a id="packages"><em>Packages</em></a>.
Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.10.0
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.10.0">4.10 infoleheküljel</a>.

<a id="source_code"></a>
Täieliku 4.10.0lähtekoodi võib vabalt alla laadida <a href="http://download.kde.org/stable/4.10.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.10.0kompileerimiseks ja paigaldamiseks
leiab samuti <a href="/info/4.10.0#binary">4.10.0infoleheküljelt.

#### Nõuded süsteemile

Võimaldamaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.8.4. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes.

## Täna ilmusid veel:

<h2> <a href="../plasma"><img src="/announcements/4/4.10.0/images/plasma.png" class="app-icon float-left mr-3" alt="The KDE Plasma Workspaces 4.10" />
Plasma töötsoonid 4.10 parandavad mobiilsete seadmete toetust ja näevad kaunimad välja
 </a></h2>

Mitmed Plasma töötsoonide komponendid porditi Qt Quick/QML-i raamistikule. Stabiilsus ja kasutatavus on paranenud. Lisandunud on uus trükkimishaldur ja värvihalduse toetus.

<h2> <a href="../platform"><img src="/announcements/4/4.10.0/images/platform.png" class="app-icon float-left mr-3" alt="The KDE Development Platform 4.10"/>
 KDE platvorm 4.10 viib rohkem API-sid üle Qt Quickile
 </a></h2>

Käesolev väljalase muudab veel lihtsamaks arendada KDE-d Plasma tarkvaraarenduse tööriistakomplektiga (SDK), kirjutada Plasma vidinaid ja vidinakogusid Qt märkekeeles (QML). Samuti võib märkida muudatusi teegis libKDEGames ja uusi skriptimisvõimalusi aknahalduris KWin.



