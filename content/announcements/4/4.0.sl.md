---
aliases:
- ../4.0
- ../4.0.sl
date: '2008-01-11'
layout: single
title: Izšel je KDE 4.0
---

<h3 align="center">
   Projekt KDE je izdal četrto večjo različico naprednega prostega namiznega okolja
</h3>
<p align="justify">
  <strong>
    S četrto večjo različico skupnost KDE naznanja začetek obdobja KDE 4.
  </strong>
</p>

<p>
Skupnost KDE z velikim zadovoljstvom najavlja takojšnjo razpoložljivost
<a href="/announcements/4.0/">KDE 4.0.0</a>. Ta pomembna izdaja
naznanja konec dolgega in intenzivnega razvojnega cikla, ki je privedel do KDE 4.0,
ter začetek obdobja serije KDE 4.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Namizje KDE 4.0</em>
</div>
<br/>


<p>
<strong>Programske knjižnice</strong> so doživele ogromno sprememb na skoraj vseh področjih.
Ogrodje Phonon prinaša vsem programom v KDE podporo za večpredstavnost, ki ni odvisna od
platforme. Solid je ogrodje za integracijo s strojno opremo, ki poenostavlja rokovanje z 
(odstranljivimi) napravami in prinaša orodja za boljše upravljanje s porabo energije.
<p />
<strong>Namizje</strong> je pridobilo veliko novih zmožnosti. Namizna lupina Plasma ponuja
nov vmesnik namizja, vključno s pultom, meniji in gradniki na namizju. Nova je tudi funkcija
armaturne plošče. KDE-jev upravljalnik oken KWin sedaj podpira napredne grafične učinke,
ki poenostavijo delo z okni na namizju.
<p />
Veliko izboljšav so bili deležni tudi <strong>programi</strong>. Lepši izgled zaradi uporabe 
vektorske grafike SVG, spremembe v podpornih programskih knjižnicah, izboljšave uporabniškega
vmesnika, nove zmožnosti in celo povsem novi programi ‒‎ vse to in še več najdemo v KDE 4.0.
Okular, nov pregledovalnik dokumentov, in Dolphin, nov upravljalnik datotek, sta le dva izmed
programov, ki izkoriščata nove tehnologije v KDE 4.0.
<p />
<img src="/announcements/4/4.0/images/oxybann.png" align="right" hspace="10"/>
<strong>Grafična podoba</strong> Oxygen prinaša svežino na namizje. Skoraj vsi vidni
deli namizja KDE in programov samih imajo posodobljen izgled. Lepota in složnost sta
dva osnovna kocepta, ki usmerjata ekipo umetnikov Oxygena.
</p>



<h3>Namizje</h3>
<ul>
        <li>Plasma je nova namizna lupina. Plasma ponuja pult, meni in ostale
          priročne načine za delo z namizjem in programi.
        </li>
        <li>KWin, KDE-jev preizkušen upravljalnik oken, sedaj podpira napredne grafične učinke.
          Strojno pospešeno izrisovanje omogoča bolj gladko in intuitivno delo z okni.
        </li>
        <li>Oxygen je nova grafična podoba KDE 4.0. Oxygen prinaša konsistentno podobo, ki
          je čudovita in prijetna na pogled.
        </li>
</ul>
Več o novem namizju KDE-ja lahko izveste v <a href="./guide">Slikovnem vodiču
po KDE 4.0</a>.

<h3>Programi</h3>
<ul>
        <li>Konqueror je KDE-jev preizkušen spletni brskalnik. Konqueror ne potrebuje veliko
          sistemskih sredstev, je dobro integriran z namizjem in podpira najnovejše spletne standarde,
          kakršen je npr. CSS 3.
        </li>
        <li>Dolphin je KDE-jev upravljalnik datotek. Dolphin je bil razvit z mislijo na čimboljšo
          uporabnost in je preprost za uporabo. Kljub temu je zmogljivo orodje.
        </li>
        <li>S Sistemskimi nastavitvami je vpeljan nov vmesnik za nadzorno središče. Sistemski
          nadzornik KSysGuard omogoča preprosto nadziranje sistemskih sredstev in aktivnosti.
        </li>
        <li>Pregledovalnik dokumentov Okular podpira mnogo različnih vrst datotek. Okular je en
          izmed mnogih programov, ki so bili izboljšani v sodelovanju s
          <a href="http://openusability.org">projektom OpenUsability</a>.
        </li>
        <li>Izobraževalni programi so bili med prvimi, ki so pričeli uporabljati nove tehnologije
          v KDE 4.0. Grafični periodni sistem elementov Kalzium in namizni globus Marble sta samo
          dva dragulja iz množice izobraževalnih programov. Več o izobraževalnih programih si lahko
          preberete v <a href="./education">Slikovnem vodiču po KDE 4.0</a>.
        </li>
        <li>Posodobljenih je bilo mnogo iger za KDE. Igre, kot sta KMines (minolovec) in KPat
          (pasjansa), imajo izboljšan izgled. Zahvaljujoč uporabi vektorske grafike SVG in novim
          grafičnim zmožnostim so igre manj odvisne od ločljivosti zaslona.
        </li>
</ul>
Nekateri programi so podrobneje predstavljeni v <a href="./guide">Slikovnem vodiču po KDE 4.0</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Upravljalnik datotek, Sistemske nastavitve in meni</em>
</div>
<br/>


<h3>Programske knjižnice</h3>
<p>
<ul>
        <li>Phonon omogoča programom večpredstavnostne zmožnosti, npr. predvajanje zvoka in videa.
          V ozadju Phonon uporablja različne hrbtenice, med katerimi je mogoče preklapljati med delovanjem.
          Privzeta hrbtenica za KDE 4.0 bo Xine, ki ponuja odlično podporo za različne vrste datotek.
          Phonon omogoča uporabniku izbiro izhodnih naprav glede na vrsto večpredstavnosti.
        </li>
        <li>Ogrodje za integracijo s strojno opremo, Solid, povezuje fiksne in odstranljive naprave
          s programi. Solid služi tudi kot vmesnik do sistemskega upravljanja s porabo energije,
          nadzoruje povezovanje v omrežja in z napravami Bluetooth. V ozadju Solid združuje
          moč storitev HAL, NetworkManager in sklada Bluez za Bluetooth. Te komponente pa je moč
          nadomestiti, brez da bi s tem zmotili programe. Na ta način je zagotovljena prenosljivost.
        </li>
        <li>KHTML je pogon za izrisovanje spletnih strani, ki ga uporablja KDE-jev spletni brskalnik
          Konqueror. KHTML ima nizko porabo sistemskih sredstev in podpira moderne spletne standarde,
          kakršen je CSS 3. KHTML je bil tudi prvi pogon, ki je prestal famozni test Acid 2.
        </li>
        <li>Knjižnica ThreadWeaver, ki je del kdelibs, ponuja visoko-nivojski vmesnik za boljšo uporabo
          današnjih procesorjev z več jedri. Na ta način lahko programi KDE delujejo bolj tekoče in
          bolj učinkovito izkoriščajo sredstva, ki jih ponuja računalnik.
        </li>
        <li>KDE 4.0 je zgrajen na osnovi Trolltech-ove knjižnice Qt 4 in tako lahko izkoristi napredne
          grafične zmožnosti in porabi manj pomnilnika. Kdelibs predstavlja odlično razširitev knjižnice
          Qt in programerju ponuja dodatno visoko-nivojsko funkcionalnost ter priročnost.
        </li>
</ul>
</p>
<p>KDE-jeva zakladnica znanja <a href="http://techbase.kde.org">TechBase</a> vsebuje dodatne podatke o
knjižnicah KDE.</p>


<h4>Odpravite se na vodeni ogled ...</h4>
<p>
<a href="./guide">Slikovni vodič po KDE 4.0</a> ponuja kratek pregled mnogih novih in
izboljšanih tehnologij v KDE 4.0. Opremljen s kopico zaslonskih posnetkov vas popelje
skozi različne dele KDE 4.0 in vam pokaže nekaj razburljivih novih tehnologij in izboljšav
namenjenih uporabniku. Začnete z <a href="./desktop">namizjem</a>, zatem so predstavljeni
<a href="./applications">programi</a>. Ne manjkajo niti <a href="./education">izobraževalni
programi</a> in <a href="./games">igre</a>.
</p>


<h4>Preizkusite ga ...</h4>
<p>
Tisti, ki bi radi testirali ter prispevali k razvoju, lahko pakete za KDE
4.0 dobijo pri svoji dristribuciji Linuxa. Nekatere imajo pakete na voljo
že na dan izida, druge jih bodo imele na voljo kmalu po tem. Celoten in
posodobljen seznam lahko vidite na <a href="http://www.kde.org/info/4.0.php">strani
z informacijami o KDE 4.0</a>. Tu lahko najdete tudi povezave do izvorne kode,
navodila za prevajanja izvorne kode, podatke o varnosti in drugo.
</p>
<p>
Sledeče distribucije so nas obvestile o razpoložljivosti paketov ali živih CD-jev za KDE 4.0:

<ul>
        <li>
                Alfa različica <strong>Arklinux 2008.1</strong>, ki temelji na KDE 4.0, je pričakovana
                kmalu po tem izidu. Končna izdaja je pričakovana po nadaljnih 3 ali 4 tednih.
        </li>
        <li>
                <strong>Debian</strong> ima pakete za KDE 4.0 v rzličici »experimental«.
                Razvojna platforma KDE bo na voljo že v različici <em>Lenny</em>. Spremljajte
                najave <a href="http://pkg-kde.alioth.debian.org/">Debian-ove ekipe za KDE</a>.
        </li>
        <li>
                <strong>Fedora</strong> bo vsebovala KDE 4.0 v Fedora 9, ki bo <a
                href="http://fedoraproject.org/wiki/Releases/9">izdana</a>
                v aprilu. Prva alfa različica bo na voljo po 24. januarju. Paketi
                za KDE 4.0 so v pred-alfa repozitoriju <a
                href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a>.
        </li>
        <li>
                <strong>Gentoo</strong> ponuja KDE 4.0 na strani
                <a href="http://kde.gentoo.org">kde.gentoo.org</a>.
        </li>
        <li>
                <strong>Kubuntu</strong> in Ubuntu paketi so vključeni v prihajajoči "Hardy Heron"
                (8.04) in so na voljo tudi kot posodobitev za stabilni "Gutsy Gibbon" (7.10).
                Na voljo je tudi živi CD za preizkušanje KDE 4.0.
                Več podatkov najdete v <a href="http://kubuntu.org/announcements/kde-4.0.php">
                tej najavi</a>.
        </li>
        <li>
                <strong>Mandriva</strong> bo pripravila pakete za različico 2008.
                Načrtujejo tudi izdajo živega CD-ja z razvojno različico 2008.1
        </li>
        <li>
                <strong>openSUSE</strong> paketi <a href="http://en.opensuse.org/KDE4">so na voljo</a> 
                za openSUSE 10.3 (
                <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">namestitev
                z enim klikom</a>) in openSUSE 10.2. <a href="http://home.kde.org/~binner/kde-four-live/">Živi CD
                KDE Four Live CD</a> je prav tako na voljo. KDE 4.0 bo del prihajajoče izdaje openSUSE 11.0.
        </li>
</ul>
</p>

<h2>O KDE 4</h2>
<p>
KDE 4.0 je inovativno prosto namizno okolje, ki vsebuje tudi mnogo dodatnih programov
za vsakodnevno uporabo ter za posebne namene. Plasma je nova namizna lupina, ki so jo
razvili za serijo KDE 4, in ponuja intuitiven vmesnik za delo z namizjem in s programi.
Spletni brskalnik Konqueror integrira splet z namizjem. Upravljalnik datotek Dolphin,
pregledovalnik dokumentov Okular in Sistemske nastavitve zaokrožajo osnovni nabor programov namizja.
<br />
KDE je zgrajen na osnovi knjižnic, ki omogočajo preprost dostop do virov na omrežju, z uporabo
tehnologije KIO, ter napredne grafične zmožnosti, z uporabo knjižnice Qt. Phonon in Solid, ki
sta prav tako del knjižnic KDE, dodata KDE programom ogrodje za večpredstavnost in boljšo 
povezanost s strojno opremo.
</p>
