---
aliases:
- ../announce-4.0-beta1
date: '2007-05-22'
description: KDE Project Ships First Beta Release for Leading Free Software Desktop.
title: KDE 4.0 Beta 1 Release Announcement
---

<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships First Beta Release for Leading Free Software Desktop, Codename "Cnuth"
</h3>

<p align="justify">
KDE 4.0 Beta 1 marks the stabilizing of foundations for the new major release
of KDE.
</p>

<p align="justify">
The KDE Community is happy to announce the immediate availability of the first
Beta release for KDE 4.0. This release marks the beginning of the integration
process which will bring the powerful new technologies included in the
now frozen KDE 4 libraries to the applications.
</p>
<p>
Almost two months after the foundations of KDE 4 have been laid with the first
alpha, KDE enters the stage of a full freeze of the library interface. From now
on, the applications will focus on integrating the new technology refined during
the last months, and the library developers will try to fix all bugs found
during this process. No new applications will enter the official KDE modules
and usability and accessibility work is of course an ongoing process. In the following weeks
KDE developers will be able to add features to their applications until the
next beta is released and the application features will be frozen as well.
</p>

<h2>Current status</h2>
<p>
At this moment, the codebase is still moving quickly. The new foundations are
stabilizing, but applications are still in flux. Since the <a
href="http://dot.kde.org/1183569837/">last Alpha</a>, a lot of work has been
committed. We've seen improvements all over KDE again. In the following sections
we will try to highlight a few of them.
</p>

<div class="text-center">
<a href="/announcements/4/4.0-beta1/marble_designer_plugin0.png">
<img src="/announcements/4/4.0-beta1/marble_designer_plugin0_small.png" class="img-fluid" />
</a> <br/>
<em>Marble widget being used in Qt Designer</em>
</div>
<br/>

<h3>Architectural</h3>
<p>
<strong><a href="http://edu.kde.org/marble/">Marble</a></strong> is an application showing a 
spherical earth which you can zoom and rotate. Marble is a geographical application
and widget and it is compatible with Google Earth's KMZ files), but more lightweight. Marble
uses Wikipedia for retrieval of geographical data and offers easy downloading of new
maps, views and other data. Inspite of using a combination of vector and bitmap
data, it is not slow, even without hardware accelleration in the form of OpenGL.
<a href="http://www.google.com">Google</a> sponsors three students working on
Marble through their <a href="http://code.google.com/soc/2007/">Summer of
Code</a> project.<br />
Marble also doubles as a generic geographical map widget and framework. It will
allow developers to easily show a person's location or let the user choose a
timezone by embedding it into their application. Of course, the educational
applications and the games will make use of this.
<br />

</p>
<p>
On July the 20th, the <a
href="http://code.google.com/soc/kde/appinfo.html?csaid=1EF6392A4C8AEADD">Icon/
Pixmap Cache</a> was <a
href="http://rivolaks.blogspot.com/2007/07/iconpixmap-cache-now-in-kdelibs.html"
>merged</a> by <a href="http://rivolaks.blogspot.com/">Rivo Laks</a> into the
KDE libraries. The icon cache speeds up loading icons when starting applications,
and in the future, it might contribute to performance when using fully scalable
icons and other scalable interface elements. The pixmap cache makes caching of
images rendered by the application, such as from SVG files, easy for application
developers. The result will be improved startup and (to a lesser extent) runtime
performance, already seen in the games KMines and KLines. 
</p>

<h3>Applications</h3>
<p>
Slowly, the many changes to the foundation of KDE are starting to become visible
to the users. Applications are starting to capitalize on the new architecture,
while adding features and other improvements.
</p>
<p>
Since the <a href="http://dot.kde.org/1180541665/">previous report</a> on <strong><a
href="http://kwin.kde.org">KWin</a></strong>, the KDE window manager, a lot progress has been made. Most work
has gone into new and improved effects and their configuration dialogs. Users of
low-end hardware aren't forgotten, as KWin will now automatically fallback to
XRENDER or even disable compositing in the absence of OpenGL rendering.<br />
Further, <a href="http://commit-digest.org/issues/2007-07-08/moreinfo/682079/">Integration</a> 
between <strong><a href="http://enzosworld.gmxhome.de/">Dolphin</a></strong>
the filemanager in KDE and <strong><a
href="http://konqueror.kde.org">Konqueror</a></strong>, KDE's webbrowser has been 
improved, and <strong><a
href="http://gwenview.sourceforge.net/">Gwenview</a></strong>, the image viewer 
received <a href="http://agateau.wordpress.com/2007/07/26/crop-this/">usability work</a> and
<a href="http://agateau.wordpress.com/2007/07/26/crop-this/">features</a>.
</p>

<div class="text-center">
<a href="/announcements/4/4.0-beta1/konsole-history-highlight.png">
<img src="/announcements/4/4.0-beta1/konsole-history-highlight_small.png" class="img-fluid">
</a> <br/>
<em>Konsole showing off split-screen and history highlight</em>
</div>
<br/>

<p>

In addition to lots of user interface cleanups and improvements, <strong>
<a href="http://konsole.kde.org">Konsole</a></strong> has improved automatic tab titles,
support for random background colors per-tab, clickable URLs and a new default
color scheme. Konsole now also provides hints to the terminal about the color
scheme being used to allow programs such as Vim to adapt their palette
accordingly, improving readability for the user.

</p>

<p>
<strong><a href="http://okular.kde.org/">Okular</a></strong>, the universal document viewer 
of KDE4 introduces usability improvements, better multithreading and work on the 
print preview component.
</p>

<div class="text-center">
<a href="/announcements/4/4.0-beta1/krdc.png">
<img src="/announcements/4/4.0-beta1/krdc_small.png" class="img-fluid">
</a> <br/>
<em>The improved KRDC in action</em>
</div>
<br/>

<p>
System administrators will be happy to hear KRDC, our remote desktop
tool, has been 
<a href="http://code.google.com/soc/kde/appinfo.html?csaid=9064143E62AF5BA6">adopted</a> by 
<a href="http://uwolfer.fwo.ch/blog/">Urs Wolfer</a>. He is
rewriting KRDC, solving many longstanding issues and adding features like tabbed
view and KWallet support. Work in <a
href="http://pim.kde.org/">KDE-PIM</a> is picking up, as features from the
KDE-PIM Enterprise branch are merged. KOrganizer received a gantt-based
time line view and an Outlook-style view, and KMail incorporated the <a
href="http://www.kde-apps.org/content/show/KMail+Tagging+Patch?content=36322">
tagging patches</a>.
</p>
<p>
More <a href="http://code.google.com/soc/kde/appinfo.html?csaid=5D66C1579098460C">effort</a> 
went in KOrganizer's theming interface by Lo&#239;c Corbasson, who
is extending the theming and plugin interface and writing some example plugins
like a Wikipedia 'this day in history' one.
</p>

<h4>Next up</h4>
<p>Now the KDE libraries are rather stable, the development focus is shifting to 
finishing other components of the
desktop. One of the most notable components is <a href="http://plasma.kde.org">Plasma</a>. 
While the developers are pretty 
much ready with the infrastructure for the plasmoids, most of them are 
not yet shipped by default Development of those features is happening in KDE's source 
code repository in the playground module. Thus, you will still see good old Kicker, 
KDE's panel and taskbar when you boot up KDE 4.0 Beta 1.
<br />
Javascript support has been added to plasma, and <a
href="http://aseigo.blogspot.com/2007/07/plasma-and-mac-dashboard.html">
according to its lead developer</a> we might soon support the Mac OS X Dashboard
widgets. Superkaramba applets are supported already, Opera widgets might follow
soon.
</p>

<h4>Porting</h4>
<p>
Extragear applications like <a href="http://www.digikam.org/">DigiKam and <a
href="http://kphotoalbum.org/">KPhotoalbum</a> are <a
href="http://www.digikam.org/?q=node/245">busy porting</a> to KDE 4, and we
encourage all application authors who haven't started this yet to get going with
the <a href="http://techbase.kde.org/Development/Tutorials/KDE4_Porting_Guide">porting
guide</a>.
</p>

<h3>Get it, run it, test it...</h3>
<p>
For those interested in getting packages to test and contribute, several
distributions notified us that they will have KDE 4.0 Beta 1 packages available at or
soon after the release. The complete and current list can be found on the 
<a href="/info/3.92">KDE 4.0 Beta1 Info Page</a>.

<h3>KOffice Releases Second Alpha</h3>
 
<p>Along with KDE 4.0 Beta 1, KOffice  
<a href="http://www.koffice.org/announcements/announce-4.0-beta1/announce-2.0alpha2">releases</a>
its second alpha version.
This version is released as a technology preview to let the public
catch a gleam of what is coming in KOffice version 2.0.  It does not
have the same level of maturity as the rest of KDE 4.0 Beta 1.</p>

<div class="text-center">
<a href="/announcements/4/4.0-beta1/koffice-flakes.png">
<img src="/announcements/4/4.0-beta1/koffice-flakes_small.png" class="img-fluid">
</a> <br/>
<em>KWord, a KOffice application using Flakes of various types</em>
</div>
<br/>

<p>The infrastructural changes are enormous. KOffice version 2 series will 
take full advantage of the improved Qt 4, giving it new features like 
<a href="http://www.kdedevelopers.org/node/2882">text directionality</a> 
and improved <a href="http://www.kdedevelopers.org/node/2812">layouting</a>.  
The text rendering is also much improved, giving it a professional quality text
layout if the fonts support it.</p>
 
<p>The core of the new improvements is the revolutionary Flake library
which will allow KOffice to use any shape in any application.  A Flake
shape can be something as simple as a circle or something as complex
as a complete spreadsheet.  It is also extremely simple to create new
shapes, something that is demonstrated by the <a href="
http://code.google.com/soc/kde/appinfo.html?csaid=9B5F3C70D02AA396">Google-sponsored</a>
Summer of Code project 
<a href="http://www.valdyas.org/fading/index.cgi/hacking/musicflake1.html">Music 
Notation flake</a>, in which a student creates a music notation plugin in just 6 weeks.</p>
 
<p>Although KOffice is included in KDE 4.0 Beta 1, it has its own
release cycle, and the first version of the KOffice version 2 series
is expected <a
href="http://wiki.koffice.org/index?title=KOffice2/Schedule">around
the new year 2007-2008</a></p>



