---
title: "Donation received - Thank you!"
---

Thank you very much for your donation to the Year End 2016 fundraiser!

In case your donation qualifies for a greeting card gift we will contact you at the beginning of December at your paypal email address to ask for the design you want and address you want to send them to.

{{< i18n_var `Remember you can become a "KDE Supporting Member" by doing recurring donations. Learn more at <a href="%[1]s">%[2]s/</a>.` "https://relate.kde.org" "https://relate.kde.org" >}}

{{< i18n_var `You can see your donation on <a href="%[1]s">the Year End 2016 fundraiser page</a>.` "index.php" >}}
