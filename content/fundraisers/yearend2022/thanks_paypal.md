---
title: "Donation received - Thank you!"
---

Thank you very much for your donation to the Year End 2022 fundraiser!

Download your "I Donated" badge, display it on your social media accounts, blog posts or website, and help us spread the word!

![Katie "I Donated" badge.](badge_katie.png)

![Konqi "I Donated" badge.](badge_konqi.png)
