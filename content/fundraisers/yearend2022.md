---
title: You Make KDE Possible
layout: yearend2022
scssFiles:
- /scss/yearend2022.scss
---

Your donation allows KDE to continue developing the spectacular [Plasma desktop](/plasma-desktop/) and [all the apps you need](https://apps.kde.org/) for education, productivity, and creative work.

![](/fundraisers/yearend2022/painting_maddog.jpg)

Your donation also helps pay for the servers hosting KDE's code, websites, and the [chat](https://webchat.kde.org/#/welcome) and video conference channels that KDE contributors use to communicate with each other. In addition, it helps pay for development sprints and the yearly [Akademy](https://akademy.kde.org/) conference.

KDE wants to be able to offer users and community members better services and support. Your donation also helps KDE provide employment to the people who can help make that happen.

![KDE booth at an external event](/fundraisers/yearend2022/kdeconf.jpg)

Your donation benefits the rest of humanity as well! With it, KDE has the resources to promote a spectacular catalogue of Free Software to professionals, artists, software developers, children, public institutions, non-profits, and companies around the globe.

![Children playing with GCompris](/fundraisers/yearend2022/kdechildren.jpg)

Your donation also supports the Free Software movement itself. KDE can adopt and nurture new projects, helping them to reach maturity, and assisting new contributors to become part of the Free Software community with a career in ethical tech.

![KDE India contributors](/fundraisers/yearend2022/kdeindia.jpg)

And if you are a developer, you can benefit from KDE's [feature-rich tools and frameworks](https://develop.kde.org/). KDE's libraries make it quick and easy to build sophisticated apps for all platforms.

In the past year, the list of platforms expanded to include the [Steam Deck](https://www.steamdeck.com), a hand-held gaming device running Plasma and KDE apps!

![The steamdeck](/fundraisers/yearend2022/steamdeck.png)

You can see even more [here](/hardware).
