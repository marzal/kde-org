---
version: "22.11.90"
date: 2022-11-25
title: "22.11.90 Releases Source Info Page"
type: info/release
build_instructions: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
signer: Heiko Becker
signing_fingerprint: D81C0CB38EB725EF6691C385BB463350D6EF31EF
---
