KDE Project Security Advisory
=============================

Title:          Windows: Incorrect behavior of uninstall.exe
Risk Rating:    Important
Versions:       KDE Windows Applications created before 9th of July 2019
Date:           21st July 2019


Overview
========
When a Windows KDE Application was installed using some non-default
parameters (local install only, using a custom install folder), the
application uninstaller was deleting all user data in the installation folder.
For example, if installed in the user's "Documents" folder (i.e. not in
"Documents/ApplicationName" but directly in "Documents"), the uninstaller
would remove the whole "Documents" folder instead of just the files added
when installing.

Workaround
==========
Do not use the uninstall.exe utility in KDE Applications created before 10th of
July 2019, simply delete the installed files and folders manually.


Credits
=======
Thanks to Hannah von Reth for the fix.
