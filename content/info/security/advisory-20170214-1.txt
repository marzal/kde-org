KDE Project Security Advisory
=============================

Title:          Kopete: XMPP User Impersonation Vulnerability
Risk Rating:    Important
CVE:            CVE-2017-5593
Versions:       kopete >= 16.11.80
Date:           14 February 2017

Overview
========

Kopete includes libiris code that is vulnerable to CVE-2017-5593.

You can find the description of CVE-2017-5593 at 
http://seclists.org/oss-sec/2017/q1/373


Solution
========

Update to Kopete >= 16.12.3 (when released)

Or apply the following patch:
https://commits.kde.org/kopete/6243764c4fd0985320d4a10b48051cc418d584ad

Credits
=======

Thanks to Pali Rohár for patching Kopete's copy of libiris code.
