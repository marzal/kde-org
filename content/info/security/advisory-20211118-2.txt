Title:          KMail: Endless loop, if the TLS certificate marked as bad
Risk Rating:    Low
Versions:       KMail, ksmtp < 5.18.1, kimap < 5.19.0, kdepim-runtime < 5.18.1
Author:         Sandro Knauß <sknauss@kde.org>
Date:           18 November 2021

Overview
========

When the IMAP TLS certificate is bad, e.g. self-signed, kmail shows a warning with
three buttons: "Details", "Continue" and "Cancel". When the user clicks on "Cancel",
kmail repeats the login process and shows the warning again immediately. 
This process continues in a loop, which can not be canceled by the user when
clicking on "Cancel" (the only secure option).

The only way to "escape" from this loop is to click on "Continue.",
which might reveal the username and password.

Impact
======

The user is "forced" to accept a bad certificate. That opens a door for attacks.

Workaround
==========

Disable network connection and then remove/disable the the failing account.

Solution
========

Update to ksmtp >= 5.18.1
Update to kimap >= 5.19.0 (when released)
Update to kdepim-runtime >= 5.18.1


Or apply at least following patches:
https://commits.kde.org/ksmtp/fca378d55e223944ce512c9a8f8b789d1d3abcde
https://commits.kde.org/kimap/7ee241898bc225237b3475f6c109ffc55a4a74c0
https://commits.kde.org/kimap/cbd3a03bc1d2cec48bb97570633940bbf94c34fa
https://commits.kde.org/kdepim-runtime/edb7f6fdea2c9f44085a042531f56223f3fd8a2f


Credits
=======

Thanks to the NO STARTTLS (https://nostarttls.secvuln.info/) team:
Damian Poddebniak, Fabian Ising, Hanno Böck, and Sebastian Schinzel
for reporting the issue.
Thanks to Volker Krause fixing the issue.
