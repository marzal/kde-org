<li>Okular in KDE SC versions up to and including 4.5.0 have a boundary 
error when processing PDB files that can be exploited to cause a
heap-based buffer overflow.
Read the <a href="security/advisory-20100825-1.txt">detailed advisory</a>
for more information.
</li>
