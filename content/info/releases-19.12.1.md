---
version: "19.12.1"
title: "19.12.1 Releases Source Info Page"
announcement: /announcements/releases/19.12
type: info/release
build_instructions: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
