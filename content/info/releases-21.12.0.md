---
version: "21.12.0"
date: 2021-09-12
title: "21.12.0 Releases Source Info Page"
type: info/release
build_instructions: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
signer: Albert Astals Cid
signing_fingerprint: 8692A42FB1A8B666C51053919D17D97FD8224750
---
