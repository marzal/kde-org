---
images:
- /thumbnail.png
highlight_apps:
- name: kdenlive
  home: https://kdenlive.org
  screen: https://cdn.kde.org/screenshots/kdenlive/k1.png
- name: kontact
  home: https://kontact.kde.org
  screen: https://cdn.kde.org/screenshots/kmail/kmail.png
- name: kdevelop
  home: https://kdevelop.org
  screen: https://www.kdevelop.org/sites/www.kdevelop.org/files/inline-images/kdevelop5-breeze_2.png
- name: gcompris
  home: https://gcompris.net
  screen: https://cdn.kde.org/screenshots/gcompris/gcompris.png
- name: labplot2
  home: https://labplot.kde.org/
  screen: https://labplot.kde.org/wp-content/uploads/2022/02/01_basic_plots_linux.png
- name: krita
  home: https://krita.org
  screen: https://krita.org/wp-content/uploads/2019/08/krita-ui-40.png
---
